trigger GHI_DR_TemplateVersionTrigger on GHI_DR_Template_Version__c (before insert, after insert, before update, after update) {
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            GHI_DR_TemplateVersionUtil.handleBeforeInsert(Trigger.new);
        } else if(Trigger.isUpdate) {
            GHI_DR_TemplateVersionUtil.handleBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
    } else if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            GHI_DR_TemplateVersionUtil.handleAfterInsert(Trigger.new, Trigger.oldMap);
        } else if(Trigger.isUpdate) {
            GHI_DR_TemplateVersionUtil.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
}