trigger GHI_DR_ResultSetTrigger on GHI_DR_Result_Set__c (before insert, before update, after insert, after update, before delete) {

    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            GHI_DR_ResultSetUtil.handleBeforeInsert(Trigger.new);
        } else if(Trigger.isUpdate) {
            GHI_DR_ResultSetUtil.handleBeforeUpdate(Trigger.new, Trigger.oldMap);
        } else if(Trigger.isDelete) {
            GHI_DR_ResultSetUtil.handleBeforeDelete(Trigger.old);
        }
    } else if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            GHI_DR_ResultSetUtil.handleAfterInsert(Trigger.new);
        } else if(Trigger.isUpdate) {
            GHI_DR_ResultSetUtil.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }

}