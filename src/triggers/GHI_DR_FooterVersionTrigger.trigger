trigger GHI_DR_FooterVersionTrigger on GHI_DR_Template_Footer_Version__c (before insert, before update, after insert, after update) {
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            GHI_DR_FooterVersionUtil.handleBeforeInsert(Trigger.new);
        } else if(Trigger.isUpdate) {
            GHI_DR_FooterVersionUtil.handleBeforeUpdate(Trigger.new);
        }
    } else if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            GHI_DR_FooterVersionUtil.handleAfterInsert(Trigger.new, Trigger.oldMap);
        } else if(Trigger.isUpdate) {
            GHI_DR_FooterVersionUtil.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
}