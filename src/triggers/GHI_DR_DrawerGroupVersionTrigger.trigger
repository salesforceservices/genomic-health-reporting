trigger GHI_DR_DrawerGroupVersionTrigger on GHI_DR_Drawer_Group_Version__c (before insert, before update, after update) {
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            GHI_DR_DrawerGroupVersionUtil.handleBeforeInsert(Trigger.new);
        } else if(Trigger.isUpdate) {
            GHI_DR_DrawerGroupVersionUtil.handleBeforeUpdate(Trigger.new);
        }
    } else if(Trigger.isAfter) {
        if(Trigger.isUpdate) {
            GHI_DR_DrawerGroupVersionUtil.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
}