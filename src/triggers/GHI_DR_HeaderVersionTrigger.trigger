trigger GHI_DR_HeaderVersionTrigger on GHI_DR_Template_Header_Version__c (before insert, before update, after insert, after update) {
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            GHI_DR_HeaderVersionUtil.handleBeforeInsert(Trigger.new);
        } else if(Trigger.isUpdate) {
            GHI_DR_HeaderVersionUtil.handleBeforeUpdate(Trigger.new);
        }
    } else if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            GHI_DR_HeaderVersionUtil.handleAfterInsert(Trigger.new, Trigger.oldMap);
        } else if(Trigger.isUpdate) {
            GHI_DR_HeaderVersionUtil.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
}