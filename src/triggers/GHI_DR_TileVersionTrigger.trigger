trigger GHI_DR_TileVersionTrigger on GHI_DR_Tile_Version__c (before insert, before update, after insert, after update) {
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            GHI_DR_TileVersionUtil.handleBeforeInsert(Trigger.new);
        } else {
            GHI_DR_TileVersionUtil.handleBeforeUpdate(Trigger.new);
        }
    } else if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            GHI_DR_TileVersionUtil.handleAfterInsert(Trigger.new, Trigger.oldMap);
        } else if(Trigger.isUpdate) {
            GHI_DR_TileVersionUtil.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
}