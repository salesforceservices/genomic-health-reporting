trigger GHI_DR_DrawerItemTrigger on GHI_DR_Drawer_Item__c (after update) {
    
    if(Trigger.isAfter) {
        if(Trigger.isUpdate) {
            GHI_DR_DrawerItemUtil.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }

}