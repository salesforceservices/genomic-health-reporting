trigger GHI_DR_TextVersionTrigger on GHI_DR_Text_Version__c (before insert, before update, after update) {
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            GHI_DR_TextVersionUtil.handleBeforeInsert(Trigger.new);
        } else if(Trigger.isUpdate) {
            GHI_DR_TextVersionUtil.handleBeforeUpdate(Trigger.new);
        }
    } else if(Trigger.isAfter) {
        if(Trigger.isUpdate) {
            GHI_DR_TextVersionUtil.handleAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
}