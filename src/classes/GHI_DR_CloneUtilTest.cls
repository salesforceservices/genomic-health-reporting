@isTest
private class GHI_DR_CloneUtilTest {
	
	private static testMethod void testNewTemplateVersionExtension() {
	    Test.startTest();
	    Contact con = GHI_DR_TestUtil.createPatient('Joe', 'Smith');
	    GHI_DR_Result_Set__c resultSet = GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, true);
	    
	    Account acct = GHI_DR_TestUtil.createAccount('acct1');
	    Order order1 = GHI_DR_TestUtil.createOrder('testOrder', GHI_DR_TestUtil.RESULT_TYPE_BREAST, acct.Id);
	    
	    GHI_DR_Template_Header__c testHeader = GHI_DR_TestUtil.createHeader('test-header');
	    GHI_DR_Template_Footer__c testFooter = GHI_DR_TestUtil.createFooter('test-footer');
	    GHI_DR_Template__c testTemplate = GHI_DR_TestUtil.createTemplate('test-template');
	    
	    GHI_DR_Template_Header_Version__c testHeaderVersion = 
	        GHI_DR_TestUtil.createHeaderVersion('thv', 'thvComponent', testHeader.Id);
	        
	    GHI_DR_Template_Footer_Version__c testFooterVersion = 
	        GHI_DR_TestUtil.createFooterVersion('tfv', 'tfvComponent', testFooter.Id);
	        
	    GHI_DR_Template_Version__c testTemplateVersion = 
	        GHI_DR_TestUtil.createTemplateVersion('ttv', testTemplate.Id, testHeaderVersion.Id, testFooterVersion.Id);
	    
	    testTemplateVersion.Approval_Status__c = 'Approved';
	    update testTemplateVersion;
	    
	    Test.stopTest();
	    
	    Test.setCurrentPage(new PageReference('/apex/GHI_DR_WebTemplate?rid=' + resultSet.Id + '&tid=' + testTemplateVersion.Id));
	    
        GHI_DR_TemplateController ctrl = new GHI_DR_TemplateController();
        System.assertEquals(testTemplateVersion.Id, ctrl.templateId);
	}	
	
	private static testMethod void testGetTemplateInfoNull() {
        GHI_DR_TemplateInfo info = GHI_DR_TemplateController.getTemplate(null, null);
        
        System.assertEquals(null, info.footer);
        System.assertEquals(null, info.header);
        System.assertEquals(null, info.template);
        System.assertEquals(null, info.tiles);
	}
	
	private static testMethod void testGetTemplateInfo() {
	    Test.startTest();
	    
	    GHI_DR_Template_Header__c testHeader = GHI_DR_TestUtil.createHeader('test-header');
	    GHI_DR_Template_Footer__c testFooter = GHI_DR_TestUtil.createFooter('test-footer');
	    GHI_DR_Template__c testTemplate = GHI_DR_TestUtil.createTemplate('test-template');
	    
	    GHI_DR_Template_Header_Version__c testHeaderVersion = 
	        GHI_DR_TestUtil.createHeaderVersion('thv', 'thvComponent', testHeader.Id);
	        
	    GHI_DR_Template_Footer_Version__c testFooterVersion = 
	        GHI_DR_TestUtil.createFooterVersion('tfv', 'tfvComponent', testFooter.Id);
	        
	    GHI_DR_Template_Version__c testTemplateVersion =
	        GHI_DR_TestUtil.createTemplateVersion('ttv', testTemplate.Id, testHeaderVersion.Id, testFooterVersion.Id);

        GHI_DR_Drawer_Group__c drawerGroup = GHI_DR_TestUtil.createDrawerGroup('tdg');
        GHI_DR_Drawer_Group_Version__c groupVersion = GHI_DR_TestUtil.createDrawerGroupVersion('dgv', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem1 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', GHI_DR_TemplateInfo.USE_STATIC, groupVersion.Id);
        GHI_DR_Drawer_Item__c drawerItem2 = GHI_DR_TestUtil.createDrawerItem('label2', 'value', GHI_DR_TemplateInfo.USE_STATIC, groupVersion.Id);

        GHI_DR_Drawer_Group_Version__c groupVersion2 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv2', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem3 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', GHI_DR_TemplateInfo.USE_STATIC, groupVersion2.Id);
        
        GHI_DR_Drawer_Group_Version__c groupVersion3 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv3', drawerGroup.Id);        
        
        testHeaderVersion.Drawer_Group_1__c = groupVersion.Id;
        testHeaderVersion.Drawer_Group_2__c = groupVersion2.Id;
        testHeaderVersion.Drawer_Group_3__c = groupVersion3.Id;
        update testHeaderVersion;
        
        GHI_DR_Tile__c testTile1 = GHI_DR_TestUtil.createTile('test tile 1');
        
        GHI_DR_Tile__c testTile2 = GHI_DR_TestUtil.createTile('test tile 2');
        
        GHI_DR_Tile_Version__c tv1 = GHI_DR_TestUtil.createTileVersion('tv1', 'TileComponentName1', testTile1.Id, testTemplateVersion.Id);
        GHI_DR_Tile_Version__c tv2 = GHI_DR_TestUtil.createTileVersion('tv2', 'TileComponentName2', testTile2.Id, testTemplateVersion.Id);
	    
	    Test.stopTest();
	    
	    ApexPages.StandardController ctrl = new ApexPages.StandardController(testTemplateVersion);
	    
	    GHI_DR_NewTemplateVersionExtension ext = new GHI_DR_NewTemplateVersionExtension(ctrl);
	    System.assertNotEquals(null, ext.createNewVersion());
	}

}