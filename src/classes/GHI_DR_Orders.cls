@RestResource(urlMapping='/GHI_DR_GetOrders/*')
global with sharing class GHI_DR_Orders {
    
    @HttpGet
    global static List<GHI_DR_Result_Set__c> getReports() {
        return getEligibleApplicationReportLinks();
    }
    
    public static List<Order> getCompletedOrders() {
        Set<String> userIdSet = new Set<String>();
        User currentUser = GHI_Portal_Utilities.getCurrentUser();
        
        List<User> users = 
            [SELECT Id 
               FROM User 
              WHERE ContactId IN (SELECT OSM_HCP__c 
                                    FROM OSM_Delegate__c 
                                   WHERE OSM_Contact__c = :currentUser.ContactId)];
        
        for(User temp : users) {
            userIdSet.add(temp.Id);
        }
        
        userIdSet.add(UserInfo.getUserId()); 
        
        return
            [SELECT Id,
                    (SELECT Id,
                            OSM_OLI_ID__r.OSM_Product_Name__c,
                            OSM_OLI_ID__r.OSM_Nodal_Status__c,
                            OSM_OLI_ID__r.OSM_Result_Current__r.OSM_Corrected_Type__c,
                            OSM_OLI_ID__r.OSM_Result_Current__r.OSM_Result_Disposition__c,
                            OSM_Distribution_Language__c,
                            OSM_OLI_ID__r.OSM_NCCN_Risk_Category__c,
                            OSM_Report_Id__c,
                            OSM_Distributed_ID__c
                       FROM Distribution_Events__r
                      WHERE OSM_Distribution_Preference__c = 'Portal' AND
                            OSM_Distribution_Status__c = 'Success' AND 
                            OSM_Report_URL__c != null
                   ORDER BY CreatedDate DESC NULLS LAST),
                   (SELECT Id,
                           Has_Comments__c,
                           Distribution_Event_Report_Id__c,
                           Patient_Name__c,
                           RecordType.Name,
                           Report_Date__c,
                           Ordering_Physician__c,
                           Report_Number__c,
                           Patient_First_Name__c,
                           Patient_Last_Name__c,
                           Digital_URL__c,
                           NCCN_Fields_Populated__c,
                           Distributed_ID__c,
                           Digital_Template__r.Language__c,
                           Digital_Template__c
                      FROM GHI_DR_Result_Sets__r)
               FROM Order
              WHERE GHI_Portal_Archived__c = false AND
                    (OSM_Status__c = 'Closed' OR OSM_Status__c = 'Canceled') AND 
                    CreatedById in :userIdSet
           ORDER BY EffectiveDate DESC];        
    }
    
    private static List<GHI_DR_Result_Set__c> getEligibleApplicationReportLinks() {
        List<GHI_DR_Result_Set__c> reports = new List<GHI_DR_Result_Set__c>();
        List<Order> listOfCompletedOrders = getCompletedOrders();
        Set<Id> uniqueReports = new Set<Id>();
        
        for(Order current : listOfCompletedOrders) {
            Map<String, GHI_DR_Result_Set__c> keyToResult = new Map<String, GHI_DR_Result_Set__c>();
            Boolean eligible = false;
            String productName;
            String correctedType;
            String nodalStatus;
            String resultDisposition;
            String distributionLanguage;
            String riskCategory;
            GHI_DR_Result_Set__c temp;
            
            for(GHI_DR_Result_Set__c result : current.GHI_DR_Result_Sets__r) {
                if(!result.Has_Comments__c && result.NCCN_Fields_Populated__c) {
                    keyToResult.put(result.Distributed_ID__c, result);
                }
            }
            
            for(OSM_Distribution_Event__c reportLink : current.Distribution_Events__r) {
                productName = reportLink.OSM_OLI_ID__r.OSM_Product_Name__c;
                nodalStatus = reportLink.OSM_OLI_ID__r.OSM_Nodal_Status__c;
                correctedType = reportLink.OSM_OLI_ID__r.OSM_Result_Current__r.OSM_Corrected_Type__c;
                resultDisposition = reportLink.OSM_OLI_ID__r.OSM_Result_Current__r.OSM_Result_Disposition__c;
                distributionLanguage = reportLink.OSM_Distribution_Language__c;
                riskCategory = reportLink.OSM_OLI_ID__r.OSM_NCCN_Risk_Category__c;
                
                eligible = 
                    correctedType != 'Corrected' && 
                    correctedType != 'Amended' &&
                    resultDisposition == 'Results' &&
                    ((productName == 'Prostate' && riskCategory != 'Unknown Risk') || 
                    (productName == 'IBC' && nodalStatus == 'Node Negative')) &&
                    keyToResult.containsKey(reportLink.OSM_Distributed_Id__c) &&
                    distributionLanguage == keyToResult.get(reportLink.OSM_Distributed_Id__c).Digital_Template__r.Language__c;
                    
                if(eligible) {
                    temp = keyToResult.get(reportLink.OSM_Distributed_Id__c);
                    if(!uniqueReports.contains(temp.Id)) {
                        reports.add(temp);
                    }
                    
                    uniqueReports.add(temp.Id);
                }
            }
        } 
        
        return reports;
    }    

}