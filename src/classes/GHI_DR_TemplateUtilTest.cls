@isTest
private without sharing class GHI_DR_TemplateUtilTest {

    private static testMethod void testSetEligiblePortalReportLinks() {
        List<User> integrationUsers =
            [SELECT Id FROM User WHERE GHI_DR_Integration_User__c = true LIMIT 1];

        List<User> communityUser = 
            [SELECT Id, ContactId 
               FROM User 
              WHERE Profile.Name = 'GHI Customer Community Plus'
              LIMIT 1];

        List<PermissionSet> permSets = 
            [SELECT Id from PermissionSet WHERE Name = 'GHI_DR_Portal_User'];

        List<PermissionSetAssignment> permSetAssignments =
            [SELECT Id 
               FROM PermissionSetAssignment 
              WHERE AssigneeId = :communityUser.get(0).Id AND 
                    PermissionSetId = :permSets.get(0).Id];

        System.assertNotEquals(0, integrationUsers.size());
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        Product2 testProd;
        Id standardPricebookId = Test.getStandardPricebookId();
        PricebookEntry testPBE;
        Account testAcct;
        Order testOrder;
        
        System.runAs ( thisUser ) {
            if(!permSets.isEmpty() && permSetAssignments.isEmpty()) {
                insert new PermissionSetAssignment(
                    AssigneeId = communityUser.get(0).Id,
                    PermissionSetId = permSets.get(0).Id
                );
            }            
            
    	    testProd = OSM_DataFactory.createProduct('ibc report', true);
    	    insert testProd;
    	    
    	    testPBE = OSM_DataFactory.createPricebookEntry(testProd.Id, standardPricebookId, 1);
    	    insert testPBE;    	  
    	    
    	    testAcct = GHI_DR_TestUtil.createAccount('acme');
    	    testOrder = GHI_DR_TestUtil.createOrder('to', 'IBC', testAcct.Id, standardPricebookId);
        }        

	    
	    GHI_DR_Template_Header__c testHeader = GHI_DR_TestUtil.createHeader('test-header');
	    GHI_DR_Template_Footer__c testFooter = GHI_DR_TestUtil.createFooter('test-footer');
	    GHI_DR_Template__c testTemplate = GHI_DR_TestUtil.createTemplate('test-template', 'IBC', 'Web');
	    
	    GHI_DR_Template_Header_Version__c testHeaderVersion = 
	        GHI_DR_TestUtil.createHeaderVersion('thv', 'thvComponent', testHeader.Id);
	        
	    GHI_DR_Template_Footer_Version__c testFooterVersion = 
	        GHI_DR_TestUtil.createFooterVersion('tfv', 'tfvComponent', testFooter.Id);
	        
	    GHI_DR_Template_Version__c testTemplateVersion = 
	        GHI_DR_TestUtil.createTemplateVersion('ttv', testTemplate.Id, testHeaderVersion.Id, testFooterVersion.Id, 'GHI_DR_WebTemplate');
	    
	    Test.startTest();
	    
	    OSM_Result__c testOSMResult = GHI_DR_TestUtil.createOSMResult('Results');

	    OrderItem testItem = 
	        GHI_DR_TestUtil.createOrderItem('IBC', 
	                                        'Node Negative', 
	                                        testOrder.Id, 
	                                        testOSMResult.Id,
	                                        testPBE.Id);

        GHI_DR_Result_Set__c testResult1, testResult2;
        OSM_Distribution_Event__c  testDist1, testDist2;
        
        System.runAs(integrationUsers.get(0)) {
    	    testDist1 = 
    	        GHI_DR_TestUtil.createDistributionEvent('Portal', 
    	                                                'Success', 
    	                                                'English', 
    	                                                '/report/url', 
    	                                                'fake-report-id', 
    	                                                testOrder.Id, 
    	                                                testItem.Id,
    	                                                testOSMResult.Id,
    	                                                'test-did-1',
    	                                                communityUser.get(0).ContactId);
    
    	    testResult1 = 
    	        GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, 
    	                                       'fake-report-id',
    	                                       testOrder.Id,
    	                                       'test-did-1');
    	                                       
    	    testResult1.Digital_Template__c = testTemplateVersion.Id;
    	    update testResult1;
    	    
    	    testResult2 = 
    	        GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, 
    	                                       'fake-report-id',
    	                                       testOrder.Id,
    	                                       'test-did-2');	                                       
    
    	    testDist2 = 
    	        GHI_DR_TestUtil.createDistributionEvent('Portal', 
    	                                                'Success', 
    	                                                'English', 
    	                                                '/report/url', 
    	                                                'fake-report-id', 
    	                                                testOrder.Id, 
    	                                                testItem.Id,
    	                                                testOSMResult.Id,
    	                                                'test-did-2',
    	                                                communityUser.get(0).ContactId);
    	                                                
    	    testResult2.Digital_Template__c = testTemplateVersion.Id;
    	    update testResult2;	                                                
    	                                       
    	    //testResult1.Digital_Template__c = testTemplateVersion.Id;
    	    //update testResult1;            
        }

	    Test.stopTest();
	    
        Boolean isPilotUser = GHI_DR_TemplateUtil.determineIsPilotUser();
        
        //Test !isPilotUser case
        List<Order> orders =
            GHI_DR_TemplateUtil.setEligiblePortalReportLinks(GHI_DR_Orders.getCompletedOrders(), false);
            
        System.assertEquals(1, orders.size());
        
        for(Order ord : orders) {
            
            System.assertEquals(2, ord.Distribution_Events__r.size());
            Boolean workingLink = false;
            for(OSM_Distribution_Event__c de : ord.Distribution_Events__r) {
                workingLink = de.OSM_Report_URL__c.contains('GHI_Portal_ReportService');
                
                if(workingLink) {
                    break;
                }
            }
            
            System.assert(workingLink);
        }

        //Test isPilotUser case
	    //System.runAs(communityUser.get(0)) {
	    /*
	      Cannot query for share records as a portal user from a unit test, so parts of the
	      test are commented out here.
	    */
            orders = GHI_DR_TemplateUtil.setEligiblePortalReportLinks(GHI_DR_Orders.getCompletedOrders(), true);
                
            System.assertEquals(1, orders.size());
            
            for(Order ord : orders) {
                
                System.assertEquals(2, ord.Distribution_Events__r.size());
                Boolean workingLink = false;
                
                for(OSM_Distribution_Event__c de : ord.Distribution_Events__r) {
                    workingLink = de.OSM_Report_URL__c.contains('GHI_DR_WebTemplate');
                    
                    if(workingLink) {
                        break;
                    }
                }
                
                //System.assert(workingLink);
            } 
	    //}        
        
        //Combined into this same unit test because of record locking issues related to standard pricebook
        
        List<GHI_DR_Result_Set__c> results = GHI_DR_Orders.getReports();
        System.assertEquals(2, results.size());
        
        
        testResult1 = 
            [SELECT Distribution_Event__c FROM GHI_DR_Result_Set__c WHERE Id = :testResult1.Id];
            
        testResult2 = 
            [SELECT Distribution_Event__c FROM GHI_DR_Result_Set__c WHERE Id = :testResult2.Id];            
            
        System.assertEquals(testDist1.Id, testResult1.Distribution_Event__c);
        System.assertEquals(testDist2.Id, testResult2.Distribution_Event__c);
        
        if(!communityUser.isEmpty()) {
            System.assertEquals(2, 
                [SELECT count() FROM GHI_DR_Result_Set__Share WHERE UserOrGroupId = :communityUser.get(0).Id]);
        }
    }
    
    private static testMethod void testEncryption() {
        System.assertNotEquals(null, GHI_DR_TemplateUtil.aes256Encrypt('test'));
    }
}