public class GHI_DR_TemplateUtil {
    
    private static final Integer MAX_DIGITAL_TEMPLATES = 10000;
    private static final String  DIGITAL_REPORT_TYPE   = 'Web';
    private static final String  FAX_REPORT_TYPE       = 'Fax';
    private static final String  PRINT_REPORT_TYPE     = 'Print';
    public  static final String  STATUS_APPROVED       = 'Approved';
    
    public static List<GHI_DR_Result_Set__c> setDigitalTemplate(List<GHI_DR_Result_Set__c> results) {
        List<GHI_DR_Template_Version__c> templates =
            [SELECT Id,
                    GHI_DR_Template__r.OSM_Product__c,
                    Active_Start_Date__c,
                    Approval_Status__c, 
                    Language__c
               FROM GHI_DR_Template_Version__c 
              WHERE GHI_DR_Template__r.Format__c = :DIGITAL_REPORT_TYPE
           ORDER BY Active_Start_Date__c DESC, CreatedDate DESC  
              LIMIT :MAX_DIGITAL_TEMPLATES];
  
        Map<Id, RecordType> rtToDeveloperName = new Map<Id, RecordType>(
            [SELECT Id, DeveloperName 
               FROM RecordType
              WHERE SObjectType = 'GHI_DR_Result_Set__c']);
              
        String devName;
        Set<Id> distributionIds = new Set<Id>();
        Map<Id, OSM_Distribution_Event__c> deToLanguage = new Map<Id, OSM_Distribution_Event__c>();
        
        for(GHI_DR_Result_Set__c result : results) {
            if(result.Distribution_Event__c != null) {
                distributionIds.add(result.Distribution_Event__c);
            }
        }
        
        deToLanguage = new Map<Id, OSM_Distribution_Event__c>(
            [SELECT Id, OSM_Distribution_Language__c 
               FROM OSM_Distribution_Event__c
              WHERE Id in :distributionIds]);
        
        for(GHI_DR_Result_Set__c result : results) {
            devName = rtToDeveloperName.get(result.RecordTypeId).DeveloperName;
            
            for(GHI_DR_Template_Version__c template : templates) {
                if(template.Active_Start_Date__c <= result.Report_Date__c &&
                   template.GHI_DR_Template__r.OSM_Product__c == devName &&
                   template.Approval_Status__c == STATUS_APPROVED && 
                   deToLanguage.containsKey(result.Distribution_Event__c) &&
                   template.Language__c == deToLanguage.get(result.Distribution_Event__c).OSM_Distribution_Language__c) {
                       
                    result.Digital_Template__c = template.Id;
                }
            }
        }
        
        return results;
    }
    
    public static Id setTemplateByResults(Id reportId, String reportFormat) {
        List<GHI_DR_Result_Set__c> results =
            [SELECT Digital_Template__c,
                    Digital_Template__r.Print_Template__c,
                    Digital_Template__r.Fax_Template__c
               FROM GHI_DR_Result_Set__c 
              WHERE Id = :reportId];
            
        if(!results.isEmpty()) {
            if(reportFormat == PRINT_REPORT_TYPE) {
                return results.get(0).Digital_Template__r.Print_Template__c;
            } else if(reportFormat == FAX_REPORT_TYPE) {
                return results.get(0).Digital_Template__r.Fax_Template__c;
            }
            
            return results.get(0).Digital_Template__c;
        }
        
        return null;
    }
    
    public static Boolean determineIsPilotUser() {
        return
            [SELECT count() 
               FROM PermissionSetAssignment 
              WHERE AssigneeId = :UserInfo.getUserId() AND 
                    (PermissionSet.Name = 'GHI_DR_Portal_User' OR PermissionSet.Name = 'GHI_DR_Admin')] > 0;
    }   
    
    public static List<Order> setEligiblePortalReportLinks(List<Order> listOfCompletedOrders, Boolean isPilotUser) {
        if(isPilotUser) {
            //Security cannot be enabled for the portal -- will cause portal users to lose order access
            //Security simulated below for result set object
            Set<Id> resultIds = new Set<Id>();
            
            for(Order current : listOfCompletedOrders) {
                for(GHI_DR_Result_Set__c result : current.GHI_DR_Result_Sets__r) {     
                    resultIds.add(result.Id);
                }
            }
            
            List<GHI_DR_Result_Set__Share> sharedSets = 
                [SELECT Id, ParentId
                   FROM GHI_DR_Result_Set__Share
                  WHERE ParentId in :resultIds AND UserOrGroupId = :UserInfo.getUserId()];

            Set<Id> sharedResultSets = new Set<Id>();
            for(GHI_DR_Result_Set__Share share : sharedSets) {
                sharedResultSets.add(share.ParentId);
            }
            
            for(Order current : listOfCompletedOrders) {
                Map<String, GHI_DR_Result_Set__c> keyToURL = new Map<String, GHI_DR_Result_Set__c>();
                Boolean eligible = false;
                String productName;
                String correctedType;
                String nodalStatus;
                String resultDisposition;
                String distributionLanguage;
                String riskCategory;
                
                for(GHI_DR_Result_Set__c result : current.GHI_DR_Result_Sets__r) {
                    if(!result.Has_Comments__c && sharedResultSets.contains(result.Id) && result.NCCN_Fields_Populated__c) {
                        keyToURL.put(result.Distributed_ID__c, result);
                    }
                }
                
                for(OSM_Distribution_Event__c reportLink : current.Distribution_Events__r) {
                    productName = reportLink.OSM_OLI_ID__r.OSM_Product_Name__c;
                    nodalStatus = reportLink.OSM_OLI_ID__r.OSM_Nodal_Status__c;
                    correctedType = reportLink.OSM_OLI_ID__r.OSM_Result_Current__r.OSM_Corrected_Type__c;
                    resultDisposition = reportLink.OSM_OLI_ID__r.OSM_Result_Current__r.OSM_Result_Disposition__c;
                    distributionLanguage = reportLink.OSM_Distribution_Language__c;
                    riskCategory = reportLink.OSM_OLI_ID__r.OSM_NCCN_Risk_Category__c;
                    
                    eligible = 
                        keyToURL.containsKey(reportLink.OSM_Distributed_ID__c) &&
                        correctedType != 'Corrected' && 
                        correctedType != 'Amended' &&
                        resultDisposition == 'Results' &&
                        distributionLanguage == keyToURL.get(reportLink.OSM_Distributed_ID__c).Digital_Template__r.Language__c &&
                        ((productName == 'Prostate' && riskCategory != 'Unknown Risk') || 
                        (productName == 'IBC' && nodalStatus == 'Node Negative'));
                        
                    if(eligible) {
                        reportLink.OSM_Report_URL__c = keyToURL.get(reportLink.OSM_Distributed_ID__c).Digital_URL__c;
                    } else {
                        reportLink.OSM_Report_URL__c = 'GHI_Portal_ReportService?id=' + reportLink.Id;
                    }
                }
            }     
        } else {
            for(Order current : listOfCompletedOrders) {
                for(OSM_Distribution_Event__c reportLink : current.Distribution_Events__r) {
                    reportLink.OSM_Report_URL__c = 'GHI_Portal_ReportService?id=' + reportLink.Id + '&case=3';
                }
            }
        }  
        
        return listOfCompletedOrders;
    }  
    
    public static String aes256Encrypt(String toEncrypt) {
    	// get private key from custom settings
    	String keyStr = GHI_Portal_Report__c.getOrgDefaults().GHI_Portal_Private_Key__c;
    	
    	if(Test.isRunningTest()) {
    	    keyStr = '12345678901234567890123456789012';
    	}
    	
        //32 byte string. since characters used are ascii, each char is 1 byte.
        Blob key = Blob.valueOf(keyStr);

        Blob cipherText = Crypto.encryptWithManagedIV('AES256', key, Blob.valueOf(toEncrypt));
        
        // convert encrypted string to hex
	    String hexData = EncodingUtil.convertToHex(cipherText);
	    Blob macData = Crypto.generateMac('HmacSHA256', Blob.valueOf(hexData),key);
	    String hexMac = EncodingUtil.convertToHex(macData);
	    
	    return hexMac + hexData;        
    }
}