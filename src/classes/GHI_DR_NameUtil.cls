public with sharing class GHI_DR_NameUtil {

    private static final String DEFAULT_LANGUAGE = 'English';
    private static final Integer DEFAULT_VERSION = 1;

    public static String generateName(String parentName, String language, Integer version) {
        if(String.isBlank(language)) {
            language = DEFAULT_LANGUAGE;
        }
        
        if(version == null) {
            version = DEFAULT_VERSION;
        }
        
        return parentName + ' ~ [ ' + language + ' - ' + version + ' ]';
    }
    
    public static List<GHI_DR_Template_Version__c> generateTemplateName(List<GHI_DR_Template_Version__c> templates) {
        Set<Id> parentIds = new Set<Id>();
        
        for(GHI_DR_Template_Version__c template : templates) {
            parentIds.add(template.GHI_DR_Template__c);
        }

        List<GHI_DR_Template__c> parents =
            [SELECT Id, Name
               FROM GHI_DR_Template__c
              WHERE Id in :parentIds];   
              
        Map<Id, String> idToName = new Map<Id, String>();
        for(GHI_DR_Template__c parent : parents) {
            idToName.put(parent.Id, parent.Name);
        }
        
        for(GHI_DR_Template_Version__c template : templates) {
            template.Name = generateName(
                idToName.get(template.GHI_DR_Template__c), 
                template.Language__c, 
                Integer.valueOf(template.Version_Number__c));
        }
        
        return templates;
    }
    
    public static List<GHI_DR_Template_Header_Version__c> generateHeaderName(List<GHI_DR_Template_Header_Version__c> headers) {
        Set<Id> parentIds = new Set<Id>();
        
        for(GHI_DR_Template_Header_Version__c header : headers) {
            parentIds.add(header.GHI_DR_Template_Header__c);
        }

        List<GHI_DR_Template_Header__c> parents =
            [SELECT Id, Name
               FROM GHI_DR_Template_Header__c
              WHERE Id in :parentIds];   
              
        Map<Id, String> idToName = new Map<Id, String>();
        for(GHI_DR_Template_Header__c parent : parents) {
            idToName.put(parent.Id, parent.Name);
        }
        
        for(GHI_DR_Template_Header_Version__c header : headers) {
            header.Name = generateName(
                idToName.get(header.GHI_DR_Template_Header__c), 
                header.Language__c, 
                Integer.valueOf(header.Version_Number__c));
        }
        
        return headers;
    } 
    
    public static List<GHI_DR_Template_Footer_Version__c> generateFooterName(List<GHI_DR_Template_Footer_Version__c> footers) {
        Set<Id> parentIds = new Set<Id>();
        
        for(GHI_DR_Template_Footer_Version__c footer : footers) {
            parentIds.add(footer.GHI_DR_Template_Footer__c);
        }

        List<GHI_DR_Template_Footer__c> parents =
            [SELECT Id, Name
               FROM GHI_DR_Template_Footer__c
              WHERE Id in :parentIds];   
              
        Map<Id, String> idToName = new Map<Id, String>();
        for(GHI_DR_Template_Footer__c parent : parents) {
            idToName.put(parent.Id, parent.Name);
        }
        
        for(GHI_DR_Template_Footer_Version__c footer : footers) {
            footer.Name = generateName(
                idToName.get(footer.GHI_DR_Template_Footer__c), 
                footer.Language__c, 
                Integer.valueOf(footer.Version_Number__c));
        }
        
        return footers;
    }
    
    public static List<GHI_DR_Drawer_Group_Version__c> generateDrawerGroupNames(List<GHI_DR_Drawer_Group_Version__c> dgs) {
        Set<Id> parentIds = new Set<Id>();
        
        for(GHI_DR_Drawer_Group_Version__c dg : dgs) {
            parentIds.add(dg.GHI_DR_Drawer_Group__c);
        }

        List<GHI_DR_Drawer_Group__c> parents =
            [SELECT Id, Name
               FROM GHI_DR_Drawer_Group__c
              WHERE Id in :parentIds];   
              
        Map<Id, String> idToName = new Map<Id, String>();
        for(GHI_DR_Drawer_Group__c parent : parents) {
            idToName.put(parent.Id, parent.Name);
        }
        
        for(GHI_DR_Drawer_Group_Version__c dg : dgs) {
            dg.Name = generateName(
                idToName.get(dg.GHI_DR_Drawer_Group__c), 
                dg.Language__c, 
                Integer.valueOf(dg.Version_Number__c));
        }
        
        return dgs;
    }
    
    public static List<GHI_DR_Tile_Version__c> generateTileNames(List<GHI_DR_Tile_Version__c> tiles) {
        Set<Id> parentIds = new Set<Id>();
        
        for(GHI_DR_Tile_Version__c tile : tiles) {
            parentIds.add(tile.GHI_DR_Tile__c);
        }

        List<GHI_DR_Tile__c> parents =
            [SELECT Id, Name
               FROM GHI_DR_Tile__c
              WHERE Id in :parentIds];   
              
        Map<Id, String> idToName = new Map<Id, String>();
        for(GHI_DR_Tile__c parent : parents) {
            idToName.put(parent.Id, parent.Name);
        }
        
        for(GHI_DR_Tile_Version__c tile : tiles) {
            tile.Name = generateName(
                idToName.get(tile.GHI_DR_Tile__c), 
                tile.Language__c, 
                Integer.valueOf(tile.Version_Number__c));
        }
        
        return tiles;
    } 
    
    public static List<GHI_DR_Text_Version__c> generateTextNames(List<GHI_DR_Text_Version__c> texts) {
        Set<Id> parentIds = new Set<Id>();
        
        for(GHI_DR_Text_Version__c text : texts) {
            parentIds.add(text.GHI_DR_Text__c);
        }

        List<GHI_DR_Text__c> parents =
            [SELECT Id, Name
               FROM GHI_DR_Text__c
              WHERE Id in :parentIds];   
              
        Map<Id, String> idToName = new Map<Id, String>();
        for(GHI_DR_Text__c parent : parents) {
            idToName.put(parent.Id, parent.Name);
        }
        
        for(GHI_DR_Text_Version__c text : texts) {
            text.Name = generateName(
                idToName.get(text.GHI_DR_Text__c), 
                text.Language__c, 
                Integer.valueOf(text.Version_Number__c));
        }
        
        return texts;
    }     
}