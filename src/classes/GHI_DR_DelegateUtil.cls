public class GHI_DR_DelegateUtil {

    private static String READ_ONLY = 'Read';

    public static void handleAfterInsert(List<OSM_Delegate__c> triggerNew) {
        addDelegateShares(triggerNew);
    }
    
    public static void handleAfterUpdate(List<OSM_Delegate__c> triggerNew, Map<Id, OSM_Delegate__c> oldMap) {
        updateDelegateShares(triggerNew, oldMap);
    }
    
    public static void handleBeforeDelete(List<OSM_Delegate__c> triggerNew) {
        removeDelegateShares(triggerNew);
    }
    
    private static void addDelegateShares(List<OSM_Delegate__c> delegates) {
        List<Delegate> shareables = getSharableDelegates(delegates);

        shareables = filterSharedDelegates(shareables);
        
        if(!shareables.isEmpty()) {
            shareDelegates(shareables);
        }
    }
    
    private static void removeDelegateShares(List<OSM_Delegate__c> delegates) {
        List<Delegate> shareables = getSharableDelegates(delegates);
        
        if(!shareables.isEmpty()) {
            removeSharedDelegates(shareables);  
        }
    }   
    
    private static void updateDelegateShares(List<OSM_Delegate__c> delegates, Map<Id, OSM_Delegate__c> old) {
        removeDelegateShares(old.values());
        addDelegateShares(delegates);
    }    

    public static void removeSharedDelegates(List<Delegate> delegates) {
        Set<Id> userIds = new Set<Id>();
        Set<Id> resultIds = new Set<Id>();
        Set<Id> shareIds = new Set<Id>();
        List<GHI_DR_Result_Set__Share> deletable = new List<GHI_DR_Result_Set__Share>();
        
        for(Delegate current : delegates) {
            userIds.add(current.userId);
            resultIds.add(current.resultId);
        }
        
        List<GHI_DR_Result_Set__Share> shares =
            [SELECT Id, ParentId, UserOrGroupId 
               FROM GHI_DR_Result_Set__Share 
              WHERE ParentId in :resultIds AND 
                    UserOrGroupId in :userIds AND 
                    RowCause <> 'Owner'];
                    
        for(Delegate current : delegates) {
            for(GHI_DR_Result_Set__Share share : shares) {
                if(share.ParentId == current.resultId && share.UserOrGroupId == current.userId) {
                    shareIds.add(share.Id);
                    
                    if(shareIds.contains(share.Id)) {
                        deletable.add(share);
                    }
                }
            }
        }
        
        delete deletable;
    }
    
    public static List<Delegate> filterSharedDelegates(List<Delegate> delegates) {
        Set<Id> userIds = new Set<Id>();
        Set<Id> resultIds = new Set<Id>();
        List<Delegate> returnable = new List<Delegate>();
        
        for(Delegate current : delegates) {
            userIds.add(current.userId);
            resultIds.add(current.resultId);
        }
        
        List<GHI_DR_Result_Set__Share> shares =
            [SELECT Id, ParentId, UserOrGroupId 
               FROM GHI_DR_Result_Set__Share 
              WHERE ParentId in :resultIds AND 
                    UserOrGroupId in :userIds];
                    
        for(Delegate current : delegates) {
            Boolean keeper = true;
            
            for(GHI_DR_Result_Set__Share share : shares) {
                if(share.ParentId == current.resultId && share.UserOrGroupId == current.userId) {
                    keeper = false;
                    break;
                }
            }
            
            if(keeper) {
                returnable.add(current);
            }
        }
        
        return returnable;
    }
    
    public static void shareDelegates(List<Delegate> delegates) {
        String delegate = Schema.GHI_DR_Result_Set__Share.RowCause.Delegate__c;
        List<GHI_DR_Result_Set__Share> shares = new List<GHI_DR_Result_Set__Share>();
        
        for(Delegate current : delegates) {
            shares.add(delegateToShare(current, delegate));
        }
        
        insert shares;
    }
    
    private static GHI_DR_Result_Set__Share delegateToShare(Delegate toShare, String rowCause) {
        return new GHI_DR_Result_Set__Share(
            ParentId      = toShare.resultId,
            RowCause      = rowCause,
            UserOrGroupId = toShare.userId,
            AccessLevel   = READ_ONLY       
        );
    }
    
    private static List<Delegate> getSharableDelegates(List<OSM_Delegate__c> delegates) {
        Set<Id> hcps = new Set<Id>();
        Map<Id, Id> delegateToSponsor = new Map<Id, Id>();
        for(OSM_Delegate__c delegate : delegates) {
            if(String.isNotBlank(delegate.OSM_HCP__c)) {
                hcps.add(delegate.OSM_HCP__c);
                
                if(String.isNotBlank(delegate.OSM_Contact__c)) {
                    delegateToSponsor.put(delegate.OSM_HCP__c, delegate.OSM_Contact__c);
                }                
            }
        }
        
        Map<Id, Id> hcpToUser = new Map<Id, Id>();
        for(User current : [SELECT Id, ContactId FROM User WHERE ContactId in :hcps]) {
            hcpToUser.put(current.ContactId, current.Id);
        }
        
        Map<Id, Id> sponsorToUser = new Map<Id, Id>();
        for(User current : [SELECT Id, ContactId FROM User WHERE ContactId in :delegateToSponsor.values()]) {
            sponsorToUser.put(current.ContactId, current.Id);
        }        

        Map<Id, Set<Id>> userToOrder = new Map<Id, Set<Id>>();
        Set<Id> hcpOrderIds = new Set<Id>();
        for(Order current : [SELECT Id, CreatedById FROM Order WHERE CreatedById in :hcpToUser.values()]) {
            if(!userToOrder.containsKey(current.CreatedById)) {
                userToOrder.put(current.CreatedById, new Set<Id>());
            }
            
            hcpOrderIds.add(current.Id);
            userToOrder.get(current.CreatedById).add(current.Id);
        }

        Map<Id, Set<Id>> orderToResults = new Map<Id, Set<Id>>();
        List<GHI_DR_Result_Set__c> results = 
            [SELECT Id, Order__c
               FROM GHI_DR_Result_Set__c
              WHERE Order__c in :hcpOrderIds];
        
        Id key;
        for(GHI_DR_Result_Set__c result : results) {
            key = result.Order__c;
            
            if(key != null) {
                if(!orderToResults.containsKey(key)) {
                    orderToResults.put(key, new Set<Id>());
                }
                
                orderToResults.get(key).add(result.Id);
            }
        }
        
        Id userId;
        Id sponsorContact;
        Id sponsorUser;
        List<Delegate> shareables = new List<Delegate>();
        
        for(Id hcp : hcps) {
            userId  = hcpToUser.get(hcp);
            
            for(Id orderId : userToOrder.get(userId)) {
                if(orderToResults.containsKey(orderId)) {
                    for(Id resultId : orderToResults.get(orderId)) {
                        shareables.add(new Delegate(userId, hcp, orderId, resultId));
                        
                        if(delegateToSponsor.containsKey(hcp)) {
                            sponsorContact = delegateToSponsor.get(hcp);
                            sponsorUser = sponsorToUser.get(sponsorContact);
                            
                            shareables.add(new Delegate(sponsorUser, sponsorContact, orderId, resultId));
                        } 
                    }                
                }                
            }
        }
        
        return shareables;
    }
    
    public class Delegate {
        public Id userId    {get; set;}
        public Id contactId {get; set;}
        public Id orderId   {get; set;}
        public Id resultId  {get; set;}
        
        public Delegate(Id userId, Id contactId, Id orderId, Id resultId) {
            this.userId    = userId;
            this.contactId = contactId;
            this.orderId   = orderId;
            this.resultId = resultId;
        }
    }
    
}