public class GHI_DR_TestUtil {
    
    public static String RESULT_TYPE_BREAST   = 'IBC';
    public static String RESULT_TYPE_PROSTATE = 'Prostate';
    
    public static ESBInboundMessage__c createInboundMessage(String type, String body, String messageInstanceId) {
        ESBInboundMessage__c test = new ESBInboundMessage__c(
            IsActive__c = true,
            Process_Inbound__c = true,
            ESBMessage__c = body,
            ESBMessageType__c = type,
            ESBMessageInstanceId__c = messageInstanceId
        );
        
        insert test;
        return test;
    }
    
    public static OSM_Delegate__c createDelegate(String name, Id sponsorId, Id delegateId, Id acctId) {
        OSM_Delegate__c test = new OSM_Delegate__c(
            Name = name,
            OSM_Contact__c = sponsorId, 
            OSM_HCP__c = delegateId, 
            OSM_HCO__c = acctId
        );
        
        insert test;
        return test;
    }

    
    public static GHI_DR_Text__c createText(String name) {
        GHI_DR_Text__c test = new GHI_DR_Text__c(
            Name = name
        );
        
        insert test;
        return test;         
    }
    
    public static GHI_DR_Text_Version__c createTextVersion(String name, String text, Id parent) {
        GHI_DR_Text_Version__c test = new GHI_DR_Text_Version__c(
            Name = name,
            Text__c = text,
            GHI_DR_Text__c = parent
        );
        
        insert test;
        return test;         
    }    

    public static Contact createContact(String firstName, String lastName) {
        Contact test = new Contact(
            FirstName = firstName,
            LastName = lastName
        );
        
        insert test;
        return test;        
    }
    
    public static Account createAccount(String name) {
        Account test = new Account(
            Name = name
        );
        
        insert test;
        return test;        
    }

    public static Order createOrder(String name, String product, Id accountId, Id pricebookId) {
        Order test = new Order(
            Name = name,
            OSM_Product__c = product,
            AccountId = accountId,
            EffectiveDate = System.today(),
            GHI_Portal_Archived__c = false,
            OSM_Status__c = 'Closed',
            Status = 'New',
            Pricebook2Id = pricebookId
        );
        
        insert test;
        return test;
    }
    
    public static Order createOrder(String name, String product, Id accountId) {
        Order test = new Order(
            Name = name,
            OSM_Product__c = product,
            AccountId = accountId,
            EffectiveDate = System.today(),
            GHI_Portal_Archived__c = false,
            OSM_Status__c = 'Closed',
            Status = 'New'
        );
        
        insert test;
        return test;
    }
    
    public static OSM_Result__c createOSMResult(String disposition) {
        OSM_Result__c test = new OSM_Result__c(
            OSM_Result_Disposition__c = disposition,
            OSM_Results_Approval_Date__c = System.today()
        );
        
        insert test;
        return test;
    }
    
    public static OrderItem createOrderItem(String productType, String nodalStatus, Id orderId, Id osmResultId, Id pbeId) {
        OrderItem test = new OrderItem(
            OSM_Product_Name__c = productType,
            OSM_Nodal_Status__c = nodalStatus,
            OrderId = orderId,
            OSM_Result_Current__c = osmResultId,
            Quantity = 1,
            PriceBookEntryId = pbeId,
            UnitPrice = 1
        );
        
        insert test;
        return test;
    }
    
    public static OSM_Distribution_Event__c createDistributionEvent(String distributionPreference, 
                                                                    String distributionStatus,
                                                                    String distributionLanguage,
                                                                    String reportURL,
                                                                    String reportId,
                                                                    Id orderId,
                                                                    Id oliId,
                                                                    Id osmResultId,
                                                                    String distributedId,
                                                                    Id contactId) {
                                                           
        OSM_Distribution_Event__c test = new OSM_Distribution_Event__c(
            OSM_Distribution_Preference__c = distributionPreference,
            OSM_Distribution_Language__c = distributionLanguage,
            OSM_Distribution_Status__c = distributionStatus,
            OSM_Report_URL__c = reportURL,
            OSM_Report_Id__c = reportId,
            OSM_Order_ID__c = orderId,
            OSM_OLI_ID__c = oliId,
            OSM_Created_date__c = System.today(),
            OSM_Lab_Task_ID__c = '1',
            OSM_Results_ID__c = osmResultId,
            OSM_Distributed_ID__c = distributedId,
            OSM_Contact__c = contactId
        );
        
        insert test;
        return test;
    }

    public static GHI_DR_Result_Set__c createResultSet(String recordType, String reportId, Id orderId, String distributedId) {
        List<RecordType> types = 
            [SELECT Id FROM RecordType WHERE DeveloperName = :recordType];

        GHI_DR_Result_Set__c test = new GHI_DR_Result_Set__c(
            RecordTypeId = types.get(0).Id,
            Distribution_Event_Report_Id__c = reportId,
            Order__c = orderId,
            Distributed_ID__c = distributedId
        );
        
        insert test;
        return test;
    }

    public static GHI_DR_Result_Set__c createResultSet(String recordType, String reportId, Id orderId) {
        List<RecordType> types = 
            [SELECT Id FROM RecordType WHERE DeveloperName = :recordType];

        GHI_DR_Result_Set__c test = new GHI_DR_Result_Set__c(
            RecordTypeId = types.get(0).Id,
            Distribution_Event_Report_Id__c = reportId,
            Order__c = orderId
        );
        
        insert test;
        return test;
    }
    
    public static GHI_DR_Result_Set__c createResultSet(String recordType) {
        List<RecordType> types = 
            [SELECT Id FROM RecordType WHERE DeveloperName = :recordType];

        GHI_DR_Result_Set__c test = new GHI_DR_Result_Set__c(
            RecordTypeId = types.get(0).Id
        );
        
        insert test;
        return test;
    }
    
    public static GHI_DR_Result_Set__c createResultSet(String recordType, Boolean isTest) {
        List<RecordType> types = 
            [SELECT Id FROM RecordType WHERE DeveloperName = :recordType];

        GHI_DR_Result_Set__c test = new GHI_DR_Result_Set__c(
            RecordTypeId = types.get(0).Id,
            Is_Test__c = isTest
        );
        
        insert test;
        return test;
    }    
    
    public static Contact createPatient(String first, String last) {
        Contact test = new Contact(
            FirstName = first,
            LastName  = last
        );
        
        insert test;
        return test;        
    }
    
    public static GHI_DR_Drawer_Group__c createDrawerGroup(String name) {
        GHI_DR_Drawer_Group__c test = new GHI_DR_Drawer_Group__c(
            Name = name    
        );
        
        insert test;
        return test;        
    }
    
    public static GHI_DR_Drawer_Group_Version__c createDrawerGroupVersion(String name, Id parent) {
        GHI_DR_Drawer_Group_Version__c test = new GHI_DR_Drawer_Group_Version__c(
            Name = name,
            GHI_DR_Drawer_Group__c = parent
        );
        
        insert test;
        return test;        
    }    
    
    public static GHI_DR_Drawer_Item__c createDrawerItem(String label, String staticValue, String fieldValue, Id parent) {
        GHI_DR_Drawer_Item__c test = new GHI_DR_Drawer_Item__c(
            Label__c = label,
            Static_Value__c = staticValue,
            Field_Value__c = fieldValue,
            GHI_DR_Drawer_Group_Version__c = parent
        );
        
        insert test;
        return test;        
    }        
    
    public static GHI_DR_Template__c createTemplate(String name) {
        GHI_DR_Template__c test = new GHI_DR_Template__c(
            Name = name    
        );
        
        insert test;
        return test;
    }
    
    public static GHI_DR_Template__c createTemplate(String name, String orderable, String medium) {
        GHI_DR_Template__c test = new GHI_DR_Template__c(
            Name = name,
            Format__c = medium,
            OSM_Product__c = orderable
        );
        
        insert test;
        return test;
    }    
    
    public static GHI_DR_Template_Version__c createTemplateVersion(String name, Id parent, Id header, Id footer) {
        GHI_DR_Template_Version__c test = new GHI_DR_Template_Version__c(
            Name = name,
            GHI_DR_Template__c = parent,
            GHI_DR_Template_Footer_Version__c = footer,
            GHI_DR_Template_Header_Version__c = header,
            Language__c = 'English'
        );
        
        insert test;
        return test;
    }   
    
    public static GHI_DR_Template_Version__c createTemplateVersion(String name, Id parent, Id header, Id footer, String baseComponent) {
        GHI_DR_Template_Version__c test = new GHI_DR_Template_Version__c(
            Name = name,
            GHI_DR_Template__c = parent,
            GHI_DR_Template_Footer_Version__c = footer,
            GHI_DR_Template_Header_Version__c = header,
            Base_Component__c = baseComponent,
            Language__c = 'English'
        );
        
        insert test;
        return test;
    }       
    
    public static GHI_DR_Template_Footer__c createFooter(String name) {
        GHI_DR_Template_Footer__c test = new GHI_DR_Template_Footer__c(
            Name = name    
        );
        
        insert test;
        return test;
    }
    
    public static GHI_DR_Template_Footer_Version__c createFooterVersion(String name, String componentName, Id parent) {
        GHI_DR_Template_Footer_Version__c test = new GHI_DR_Template_Footer_Version__c(
            Name = name,
            GHI_DR_Template_Footer__c = parent
        );
        
        insert test;
        return test;
    }
    
    public static GHI_DR_Template_Header__c createHeader(String name) {
        GHI_DR_Template_Header__c test = new GHI_DR_Template_Header__c(
            Name = name    
        );
        
        insert test;
        return test;
    }
    
    public static GHI_DR_Template_Header_Version__c createHeaderVersion(String name, String componentName, Id parent) {
        GHI_DR_Template_Header_Version__c test = new GHI_DR_Template_Header_Version__c(
            Name = name,
            GHI_DR_Template_Header__c = parent
        );
        
        insert test;
        return test;
    }
    
    public static GHI_DR_Tile__c createTile(String name) {
        GHI_DR_Tile__c test = new GHI_DR_Tile__c(
            Name = name    
        );
        
        insert test;
        return test;        
    }
    
    public static GHI_DR_Tile_Version__c createTileVersion(String name, String componentName, Id parent, Id templateId) {
        GHI_DR_Tile_Version__c test = new GHI_DR_Tile_Version__c(
            Name = name,
            GHI_DR_Tile__c = parent,
            GHI_DR_Template_Version__c = templateId
        );
        
        insert test;
        return test;          
    }
    
    public static String getInboundMessageBreastXML() {
        String xml = '<ns0:ResultsDataMessage ackRequired="false" activityId="efc480ae-88b5-8770-7b94-fbefb50a1ac1" correlationId="5aca1e8c-e341-4b0a-8fc9-e2075d969afc" destination="SFDC" expires="2015-09-09T22:08:51Z" messageInstanceId="4b12543d-8fa2-4daf-879d-57a55e41b061" nackRequired="true" origin="ESB" timestamp="2015-09-09T18:08:51Z" xmlns:ns0="http://schemas.genomichealth.com/Internal/Crm/v1/ResultsDataMessage.xsd" xmlns:env="http://schemas.genomichealth.com/Internal/Esb/v1/Envelope.xsd" xmlns:commonTypes="http://schemas.genomichealth.com/Internal/Common/v1/CommonTypes.xsd"><OrderId>OR000055712</OrderId><ReportId>OR000055712-01</ReportId><Report type="OncoTypeRC" disclaimerversion="4.0"><ReportDate>2015-09-09</ReportDate><ACReportDate>2015-09-09</ACReportDate><ReportID>OR000055712-01</ReportID><ChangeType></ChangeType><Order><Status>Processing</Status><OrderableType>IBC</OrderableType><MedicalRecordNumber>12345678</MedicalRecordNumber><OrderableName>IBC</OrderableName><StudyName></StudyName><OrderNumber>OR000055712</OrderNumber><ReportType>Single</ReportType><Patient><Name><FirstName>Jane</FirstName><LastName>Doe</LastName></Name><Gender>Female</Gender><DOB>1950-01-01</DOB><LegacyId>12345678</LegacyId><Phone>(650) 556-9300</Phone><Email></Email></Patient><Account><AcctName>Strawberry Fields Medical Center</AcctName></Account><OrderLineItem workOrderNumber="WO000001190" orderLineItemNumber="OL000004516" testType="IBC" modified="None"><ReportDate>2015-09-09</ReportDate><LabDirectors>Patrick Joseph, MD</LabDirectors><Specimen><SpecimenID>OR000055712-ES</SpecimenID><Franchise>Breast</Franchise><CollectDate>9-9-2015</CollectDate><SubmittingDiagnosis>Malignant</SubmittingDiagnosis><RecvDate>2015-09-09</RecvDate><ERStatus>Positive</ERStatus><NodalStatus>Node_Negative</NodalStatus></Specimen><ResultSet type="Calculated" Source="Internal" status="Pass" approvedBy="Huu Huyen" generatedOn="2015-09-09"><Result name="Recurrence" type="Numeric" unrounded="42.2" lowRange="0" highRange="50">42</Result><Result name="RiskGroup" type="Numeric">3</Result><Result name="ERPositive" type="Boolean">True</Result><CompositeResult name="ER" type="SingleGene"><Result name="GeneValue" type="Numeric" unrounded="10.39337" lowRange="3.7" highRange="12.5">10.3</Result><Result name="Classification" type="Text">Positive</Result><Result name="Range" type="Text">InRange</Result></CompositeResult><CompositeResult name="PR" type="SingleGene"><Result name="GeneValue" type="Numeric" unrounded="6.84047" lowRange="3.2" highRange="10">6.8</Result><Result name="Classification" type="Text">Positive</Result><Result name="Range" type="Text">InRange</Result></CompositeResult><CompositeResult name="HER2" type="SingleGene"><Result name="GeneValue" type="Numeric" unrounded="12.6523" lowRange="7.6" highRange="13">12.6</Result><Result name="Classification" type="Text">Positive</Result><Result name="Range" type="Text">InRange</Result></CompositeResult><CompositeResult name="NodeNegB14" type="Risk"><Result name="Defined" type="Boolean">True</Result><Result name="Rate" type="Numeric" unrounded="28.7" lowRange="0" highRange="40" units="%">29</Result><Result name="LowerConfidence" type="Numeric" unrounded="21.3" lowRange="0" highRange="40" units="%">21</Result><Result name="UpperConfidence" type="Numeric" unrounded="35.3" lowRange="0" highRange="40" units="%">35</Result></CompositeResult></ResultSet></OrderLineItem></Order><Recipients role="Physician"><Name><FirstName>George</FirstName><MiddleName>Larry</MiddleName><LastName>Harrison</LastName></Name><Identifier>NPI123</Identifier></Recipients><Recipients role="Other"><Name><FirstName>Sid</FirstName><LastName>Moid</LastName><Salutation>Dr.</Salutation></Name><Identifier>NPI987</Identifier></Recipients></Report></ns0:ResultsDataMessage>';
    
        Record_Type__c systemSetting = Record_Type__c.getOrgDefaults();
        
        if(systemSetting.Base64_Encoding__c) {
            return EncodingUtil.base64Encode(Blob.valueOf(xml));
        }
    
        return xml;
    }
    
    public static String getInboundMessageProstateXML() {
        String xml = '<ns0:ResultsDataMessage ackRequired="false" activityId="e2a7d040-e6a4-495a-9a23-7a8ecaa162c4" correlationId="43efb218-c122-446e-aa77-b241c0d6b00e" destination="SFDC" expires="2015-09-09T23:10:14Z" messageInstanceId="dedaf28d-5c99-445d-9a04-ea150600df56" nackRequired="true" origin="ESB" timestamp="2015-09-09T19:10:14Z" xmlns:ns0="http://schemas.genomichealth.com/Internal/Crm/v1/ResultsDataMessage.xsd" xmlns:env="http://schemas.genomichealth.com/Internal/Esb/v1/Envelope.xsd" xmlns:commonTypes="http://schemas.genomichealth.com/Internal/Common/v1/CommonTypes.xsd"><OrderId>OR000055714</OrderId><ReportId>OR000055714-01</ReportId><Report type="OncoTypeProstate" disclaimerversion="1.0"><ReportDate>2015-09-09</ReportDate><ACReportDate>2015-09-09</ACReportDate><ReportID>OR000055714-01</ReportID><ChangeType></ChangeType><Order><Status>Processing</Status><OrderableType>Prostate</OrderableType><MedicalRecordNumber>12345678</MedicalRecordNumber><OrderableName>Prostate</OrderableName><StudyName></StudyName><OrderNumber>OR000055714</OrderNumber><ReportType>Single</ReportType><Patient><Name><FirstName>John</FirstName><LastName>Doe</LastName></Name><Gender>Male</Gender><DOB>1955-01-01</DOB><LegacyId>12345678</LegacyId><Phone>(650) 556-9300</Phone><Email></Email></Patient><OrderLineItem workOrderNumber="WO000001191" orderLineItemNumber="OL000004518" testType="Prostate" modified="None"><ReportDate>2015-09-09</ReportDate><LabDirectors>Patrick Joseph, MD</LabDirectors><Specimen><SpecimenID>OR000055714-ES</SpecimenID><Franchise>Prostate</Franchise><CollectDate>9-9-2015</CollectDate><SubmittingDiagnosis>Prostate Cancer</SubmittingDiagnosis><RecvDate>2015-09-09</RecvDate><NCCNRiskCategory>Intermediate_Risk</NCCNRiskCategory></Specimen><ResultSet type="Calculated" Source="Internal" status="Pass" approvedBy="Huu Huyen" generatedOn="2015-09-09"><Result name="GPScore" type="Numeric" unrounded="60.40889" lowRange="0" highRange="100">60</Result><Result name="GPScoreInRange" type="Boolean">True</Result><Result name="RiskGroup" type="Numeric">3</Result><Result name="ShowGleasonScoreQualifier" type="Boolean">False</Result><Result name="IntermediateRiskNCCNExpectedRange" type="Numeric" unrounded="3" lowRange="1" highRange="3">3</Result><CompositeResult name="IntermediateRiskAdversePathology" type="Risk"><Result name="Defined" type="Boolean">True</Result><Result name="Rate" type="Numeric" unrounded="33" lowRange="30" highRange="100" units="%">33</Result><Result name="LowerConfidence" type="Numeric" unrounded="20" lowRange="30" highRange="100" units="%">20</Result><Result name="UpperConfidence" type="Numeric" unrounded="49" lowRange="30" highRange="100" units="%">49</Result></CompositeResult><CompositeResult name="IntermediateRiskHighGrade" type="Risk"><Result name="Defined" type="Boolean">True</Result><Result name="Rate" type="Numeric" unrounded="50" lowRange="30" highRange="100" units="%">50</Result><Result name="LowerConfidence" type="Numeric" unrounded="33" lowRange="30" highRange="100" units="%">33</Result><Result name="UpperConfidence" type="Numeric" unrounded="67" lowRange="30" highRange="100" units="%">67</Result></CompositeResult><CompositeResult name="IntermediateRiskNonOrgan" type="Risk"><Result name="Defined" type="Boolean">True</Result><Result name="Rate" type="Numeric" unrounded="46" lowRange="30" highRange="100" units="%">46</Result><Result name="LowerConfidence" type="Numeric" unrounded="30" lowRange="30" highRange="100" units="%">30</Result><Result name="UpperConfidence" type="Numeric" unrounded="63" lowRange="30" highRange="100" units="%">63</Result></CompositeResult></ResultSet></OrderLineItem></Order><Recipients role="Physician"><Name><FirstName>George</FirstName><MiddleName>Larry</MiddleName><LastName>Harrison</LastName></Name><Identifier>NPI123</Identifier></Recipients><Recipients role="Other"><Name><FirstName>Sid</FirstName><LastName>Moid</LastName><Salutation>Dr.</Salutation></Name><Identifier>NPI987</Identifier></Recipients></Report></ns0:ResultsDataMessage>';
        
        Record_Type__c systemSetting = Record_Type__c.getOrgDefaults();
        
        if(systemSetting.Base64_Encoding__c) {
            return EncodingUtil.base64Encode(Blob.valueOf(xml));
        }
    
        return xml;
    }    
    
}