public with sharing class GHI_DR_DrawerItemUtil {
    
    public static void handleAfterUpdate(List<GHI_DR_Drawer_Item__c> triggerNew, Map<Id, GHI_DR_Drawer_Item__c> triggerOld) {
        //enforceReadOnly(triggerNew, triggerOld);
    }    
    
    public static void enforceReadOnly(List<GHI_DR_Drawer_Item__c> items, Map<Id, GHI_DR_Drawer_Item__c> old) {
        Map<Id, Id> dgvToTemplate = new Map<Id, Id>();
        Set<Id> dgvIds = new Set<Id>();
        Set<Id> templateIds = new Set<Id>();
        
        for(GHI_DR_Drawer_Item__c item : items) {
            dgvIds.add(item.GHI_DR_Drawer_Group_Version__c);
        }
        
        for(GHI_DR_Drawer_Group_Version__c dgv : [SELECT Id, Template_Version__c 
                                                    FROM GHI_DR_Drawer_Group_Version__c 
                                                   WHERE Id in :dgvIds AND Template_Version__c != null]) {
                                                       
            dgvToTemplate.put(dgv.Id, dgv.Template_Version__c);
        }

        for(GHI_DR_Drawer_Item__c item : items) {
            if(item.GHI_DR_Drawer_Group_Version__c != null && dgvToTemplate.containsKey(item.GHI_DR_Drawer_Group_Version__c)) {
                templateIds.add(dgvToTemplate.get(item.GHI_DR_Drawer_Group_Version__c));
            }
        } 
 
        Map<Id, GHI_DR_Template_Version__c> idToTemplate = new Map<Id, GHI_DR_Template_Version__c>(
            [SELECT Id 
               FROM GHI_DR_Template_Version__c 
              WHERE Approval_Status__c = :GHI_DR_TemplateUtil.STATUS_APPROVED AND Id in :templateIds]);

        for(GHI_DR_Drawer_Item__c item : items) {
            if(item.GHI_DR_Drawer_Group_Version__c != null && idToTemplate.containsKey(dgvToTemplate.get(item.GHI_DR_Drawer_Group_Version__c))) {
                item.addError(GHI_DR_TemplateVersionUtil.ALL_RO_ERROR);
            }
        }
    }
}