public with sharing class GHI_DR_HeaderVersionUtil {
    
    public static void handleBeforeInsert(List<GHI_DR_Template_Header_Version__c> triggerNew) {
        setVersionNumber(triggerNew);
        GHI_DR_NameUtil.generateHeaderName(triggerNew);
    }
    
    public static void handleBeforeUpdate(List<GHI_DR_Template_Header_Version__c> triggerNew) {
        populateTemplateVersion(triggerNew);
        GHI_DR_NameUtil.generateHeaderName(triggerNew);
    }    
    
    public static void handleAfterInsert(List<GHI_DR_Template_Header_Version__c> triggerNew, Map<Id, GHI_DR_Template_Header_Version__c> triggerOld) {
        updateTextTemplates(triggerNew, triggerOld);
    }
    
    public static void handleAfterUpdate(List<GHI_DR_Template_Header_Version__c> triggerNew, Map<Id, GHI_DR_Template_Header_Version__c> triggerOld) {
        updateTextTemplates(triggerNew, triggerOld);
        enforceReadOnly(triggerNew, triggerOld);
    }    
    
    public static List<GHI_DR_Template_Header_Version__c> setVersionNumber(List<GHI_DR_Template_Header_Version__c> headers) {
        Set<Id> parents = new Set<Id>();
        
        for(GHI_DR_Template_Header_Version__c header : headers) {
            parents.add(header.GHI_DR_Template_Header__c);
        }

        parents.remove(null);
        
        List<AggregateResult> ars = 
            [SELECT GHI_DR_Template_Header__c pid, MAX(Version_Number__c) num, Language__c lang 
               FROM GHI_DR_Template_Header_Version__c 
              WHERE GHI_DR_Template_Header__c in :parents 
           GROUP BY GHI_DR_Template_Header__c, Language__c];
           
        Map<String, Integer> tvToCounts = new Map<String, Integer>();
        
        for(AggregateResult ar : ars) {
            tvToCounts.put((Id) ar.get('pid') + String.valueOf(ar.get('lang')), Integer.valueOf(ar.get('num')));
        }

        Integer currentVersion;
        String key;
        for(GHI_DR_Template_Header_Version__c header : headers) {
            key = header.GHI_DR_Template_Header__c + header.Language__c;
            currentVersion = tvToCounts.containsKey(key) ?
                             tvToCounts.get(key) + 1 : 1;
            
            tvToCounts.put(key, currentVersion);
                             
            header.Version_Number__c = currentVersion;
        }
        
        return headers;
    }    
    
    public static void enforceReadOnly(List<GHI_DR_Template_Header_Version__c> headers, Map<Id, GHI_DR_Template_Header_Version__c> old) {
        Set<Id> templateIds = new Set<Id>();
        for(GHI_DR_Template_Header_Version__c header : headers) {
            if(header.Template_Version__c != null) {
                templateIds.add(header.Template_Version__c);
            }
        }
        
        Map<Id, GHI_DR_Template_Version__c> idToTemplate = new Map<Id, GHI_DR_Template_Version__c>(
            [SELECT Id, Is_Template_Live__c 
               FROM GHI_DR_Template_Version__c 
              WHERE Approval_Status__c = :GHI_DR_TemplateUtil.STATUS_APPROVED AND Id in :templateIds]);

        for(GHI_DR_Template_Header_Version__c header : headers) {
            if(header.Template_Version__c != null && idToTemplate.containsKey(header.Template_Version__c)) {
                header.addError(GHI_DR_TemplateVersionUtil.ALL_RO_ERROR);
            }
        }
    }    
    
    public static List<GHI_DR_Template_Header_Version__c> updateTextTemplates(List<GHI_DR_Template_Header_Version__c> headers, 
                                                                              Map<Id, GHI_DR_Template_Header_Version__c> old) {

        Map<Id, Id> textToTemplate = new Map<Id, Id>();
        Id current;
        Id previous;

        for(GHI_DR_Template_Header_Version__c header : headers) {
            for(String field : GHI_DR_CloneUtil.TEXTS_HEADER_VERSION) {
                current  = (Id) header.get(field);
                previous = old != null && old.containsKey(header.Id) ?
                           (Id) old.get(header.Id).get(field) : null;
                
                if(current == null) {
                    if(previous != null) {
                        textToTemplate.put(previous, null);
                    }
                } else {
                    textToTemplate.put(current, header.Template_Version__c);
                }
            }
        }
        
        GHI_DR_TextVersionUtil.updateTextTemplates(textToTemplate);     
        return headers;
    }    
    
    public static List<GHI_DR_Template_Header_Version__c> populateTemplateVersion(List<GHI_DR_Template_Header_Version__c> headers) {
        List<GHI_DR_Template_Version__c> templates = 
            [SELECT Id,
                    GHI_DR_Template_Header_Version__c,
                    Language__c
               FROM GHI_DR_Template_Version__c 
              WHERE GHI_DR_Template_Header_Version__c in :headers];
        
        Map<Id, GHI_DR_Template_Version__c> drawerToTemplate = new Map<Id, GHI_DR_Template_Version__c>();
        for(GHI_DR_Template_Version__c template : templates) {
            drawerToTemplate.put(template.GHI_DR_Template_Header_Version__c, template);
        }
        
        for(GHI_DR_Template_Header_Version__c header : headers) {
            if(drawerToTemplate.containsKey(header.Id)) {
                header.Template_Version__c = drawerToTemplate.get(header.Id).Id;
                header.Language__c = drawerToTemplate.get(header.Id).Language__c;                
            }
        }
        
        return headers;
    }     
    
}