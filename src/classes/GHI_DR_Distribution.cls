public with sharing class GHI_DR_Distribution {

    private static String READ_ONLY = 'Read';

    public static void linkDistributionEvent(List<GHI_DR_Result_Set__c> triggerNew) {
        Set<String> distributedIds = new Set<String>();
        Map<String, Id> distributionMap = new Map<String, Id>();
        
        for(GHI_DR_Result_Set__c result : triggerNew) {
            if(String.isNotBlank(result.Distributed_ID__c)) {
                distributedIds.add(result.Distributed_ID__c);
            }
        }
        
        List<OSM_Distribution_Event__c> distributions = new List<OSM_Distribution_Event__c>(
            [SELECT Id, OSM_Distributed_ID__c
               FROM OSM_Distribution_Event__c
              WHERE OSM_Distributed_ID__c in :distributedIds]);
        
        for(OSM_Distribution_Event__c distribution : distributions) {
            if(String.isNotBlank(distribution.OSM_Distributed_ID__c)) {
                distributionMap.put(distribution.OSM_Distributed_ID__c, distribution.Id);
            }
        }
        
        for(GHI_DR_Result_Set__c result : triggerNew) {
            if(distributionMap.containsKey(result.Distributed_ID__c)) {
                result.Distribution_Event__c = distributionMap.get(result.Distributed_ID__c);
            }
        }
    }
    
    public static void linkDistributionEvent(List<OSM_Distribution_Event__c> triggerNew) {
        Set<String> distributedIds = new Set<String>();
        Map<String, Id> distributionMap = new Map<String, Id>();
        
        for(OSM_Distribution_Event__c distribution : triggerNew) {
            if(String.isNotBlank(distribution.OSM_Distributed_ID__c)) {
                distributedIds.add(distribution.OSM_Distributed_ID__c);
            }
        }        
        
        List<GHI_DR_Result_Set__c> results = 
            [SELECT Id, Distributed_ID__c
               FROM GHI_DR_Result_Set__c
              WHERE Distributed_ID__c in :distributedIds];
        
        for(OSM_Distribution_Event__c distribution : triggerNew) {
            if(String.isNotBlank(distribution.OSM_Distributed_ID__c)) {
                distributionMap.put(distribution.OSM_Distributed_ID__c, distribution.Id);
            }
        }         
        
        for(GHI_DR_Result_Set__c result : results) {
            if(distributionMap.containsKey(result.Distributed_ID__c)) {
                result.Distribution_Event__c = distributionMap.get(result.Distributed_ID__c);
            }
        }
        
        update results;
    }
    
    public static void distributeResultSet(List<GHI_DR_Result_Set__c> results) {
        Set<Id> distributionIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        Map<Id, Id> contactToUser = new Map<Id, Id>();
        List<GHI_DR_Result_Set__Share> shares = new List<GHI_DR_Result_Set__Share>();
        
        for(GHI_DR_Result_Set__c result : results) {
            if(result.Distribution_Event__c != null) {
                distributionIds.add(result.Distribution_Event__c);
            }
        }
        
        Map<Id, OSM_Distribution_Event__c> distributions = new Map<Id, OSM_Distribution_Event__c>( 
            [SELECT Id, OSM_Contact__c 
               FROM OSM_Distribution_Event__c
              WHERE Id in :distributionIds AND OSM_Contact__c != null]);
              
        for(OSM_Distribution_Event__c distribution : distributions.values()) {
            if(distribution.OSM_Contact__c != null) {
                contactIds.add(distribution.OSM_Contact__c);
            }
        }
        
        List<User> users =
            [SELECT Id, ContactId
               FROM User
              WHERE ContactId in :contactIds];
        
        for(User current : users) {
            contactToUser.put(current.ContactId, current.Id);
        }
        
        OSM_Distribution_Event__c tempDistribution;
        Id shareWith;
        String sponsor = Schema.GHI_DR_Result_Set__Share.RowCause.Sponsor__c;
        
        for(GHI_DR_Result_Set__c result : getUnsharedResults(results)) {
            if(result.Distribution_Event__c != null && distributions.containsKey(result.Distribution_Event__c)) {
                tempDistribution = distributions.get(result.Distribution_Event__c);
                shareWith = contactToUser.get(tempDistribution.OSM_Contact__c);
                
                if(shareWith != null) {
                    shares.add(new GHI_DR_Result_Set__Share(
                        ParentId      = result.Id,
                        RowCause      = sponsor,
                        UserOrGroupId = shareWith,
                        AccessLevel   = READ_ONLY
                    ));
                }
            }
        }        
        
        insert shares;
    }
    
    private static List<GHI_DR_Result_Set__c> getUnsharedResults(List<GHI_DR_Result_Set__c> results) {
        Set<Id> shared = new Set<Id>();
        List<GHI_DR_Result_Set__c> unshared = new List<GHI_DR_Result_Set__c>();
        String sponsor = Schema.GHI_DR_Result_Set__Share.RowCause.Sponsor__c;
        
        List<GHI_DR_Result_Set__Share> sharedSets =
            [SELECT ParentId 
               FROM GHI_DR_Result_Set__Share 
              WHERE ParentId in :results AND RowCause = :sponsor];
              
        for(GHI_DR_Result_Set__Share setShare : sharedSets) {
            shared.add(setShare.ParentId);
        }
        
        for(GHI_DR_Result_Set__c result : results) {
            if(!shared.contains(result.Id)) {
                unshared.add(result);
            }
        }
        
        return unshared;
    }
    
}