@isTest
private class GHI_DR_DelegateUtilTest {

	private static testMethod void testDelegateTrigger() {
        Id pbId = Test.getStandardPricebookId();
        Integer numUsers = 3;
        Account testAcct, testAcct2;
        List<User> communityUser = 
            [SELECT Id, ContactId 
               FROM User 
              WHERE Profile.Name = 'GHI Customer Community Plus' AND ContactId <> null
              LIMIT :numUsers];
        
        if(communityUser.size() != numUsers) {
            return;
        }
        
        User delegate    = communityUser.get(0);
        User sponsor     = communityUser.get(1);
        User altDelegate = communityUser.get(2);
        Order testOrder, testOrder2;
        System.runAs(delegate) {
            testAcct = GHI_DR_TestUtil.createAccount('Tester');
            
            testOrder = GHI_DR_TestUtil.createOrder(
                'Tester', 'Tester', testAcct.Id, pbId);
        }
        
        System.runAs(altDelegate) {
            testAcct2 = GHI_DR_TestUtil.createAccount('Tester');
            
            testOrder2 = GHI_DR_TestUtil.createOrder(
                'Tester', 'Tester', testAcct2.Id, pbId);
        }        
        
        //result set related to order String recordType, String reportId, Id orderId, String distributedId
        GHI_DR_Result_Set__c result = 
            GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, 
                                           'fake-report-id', 
                                           testOrder.Id, 
                                           'fake-dist-id');
                                           
        GHI_DR_Result_Set__c result2 = 
            GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, 
                                           'fake-report-id', 
                                           testOrder2.Id, 
                                           'fake-dist-id');                                           

        //verify result set trigger shared with owner of order
        List<GHI_DR_Result_Set__Share> shares = 
            [SELECT Id 
               FROM GHI_DR_Result_Set__Share 
              WHERE ParentId = :result.Id AND RowCause = 'Delegate__c'];
              
        System.assertEquals(1, shares.size());      
        
        //create delegate record
        OSM_Delegate__c testDelegate = GHI_DR_TestUtil.createDelegate('Tester', sponsor.ContactId, delegate.ContactId, null);
        
        //verify share exists
        shares = 
            [SELECT Id 
               FROM GHI_DR_Result_Set__Share 
              WHERE ParentId = :result.Id AND RowCause = 'Delegate__c'];
        
        System.assertEquals(2, shares.size());
        
        //update delegate record to point to someone else
        testDelegate.OSM_HCP__c = altDelegate.ContactId;
        update testDelegate;
        
        //verify old share removed, new one exists
        shares = 
            [SELECT Id 
               FROM GHI_DR_Result_Set__Share 
              WHERE ParentId = :result.Id AND RowCause = 'Delegate__c'];
        
        System.assertEquals(0, shares.size()); 
        
        shares = 
            [SELECT Id 
               FROM GHI_DR_Result_Set__Share 
              WHERE ParentId = :result2.Id AND RowCause = 'Delegate__c'];
        
        System.assertEquals(2, shares.size());         
        
        //delete delegate record, verify no share exists
        delete testDelegate;

        shares = 
            [SELECT Id 
               FROM GHI_DR_Result_Set__Share 
              WHERE ParentId = :result2.Id AND RowCause = 'Delegate__c'];
        
        System.assertEquals(0, shares.size());         
	}

	private static testMethod void testResultSetTrigger() {
        Id pbId = Test.getStandardPricebookId();
        Integer numUsers = 3;
        Account testAcct, testAcct2;
        List<User> communityUser = 
            [SELECT Id, ContactId 
               FROM User 
              WHERE Profile.Name = 'GHI Customer Community Plus' AND ContactId <> null
              LIMIT :numUsers];
              
        List<User> integrationUsers =
            [SELECT Id FROM User WHERE GHI_DR_Integration_User__c = true LIMIT 1];             
        
        if(communityUser.size() != numUsers) {
            return;
        }
        
        User delegate    = communityUser.get(0);
        User sponsor     = communityUser.get(1);
        User altDelegate = communityUser.get(2);
        Order testOrder, testOrder2;
        System.runAs(delegate) {
            testAcct = GHI_DR_TestUtil.createAccount('Tester');
            
            testOrder = GHI_DR_TestUtil.createOrder(
                'Tester', 'Tester', testAcct.Id, pbId);
        }
        
        System.runAs(altDelegate) {
            testAcct2 = GHI_DR_TestUtil.createAccount('Tester');
            
            testOrder2 = GHI_DR_TestUtil.createOrder(
                'Tester', 'Tester', testAcct2.Id, pbId);
        }        
        
        //result set related to order String recordType, String reportId, Id orderId, String distributedId
        
        //create delegate record
        OSM_Delegate__c testDelegate = GHI_DR_TestUtil.createDelegate('Tester', sponsor.ContactId, delegate.ContactId, null);
        
        GHI_DR_Result_Set__c result = 
            GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, 
                                           'fake-report-id', 
                                           testOrder.Id, 
                                           'fake-dist-id');
                                           
        GHI_DR_Result_Set__c result2 = 
            GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, 
                                           'fake-report-id', 
                                           testOrder2.Id, 
                                           'fake-dist-id');                                           
        
        //verify share exists
        List<GHI_DR_Result_Set__Share> shares = 
            [SELECT Id 
               FROM GHI_DR_Result_Set__Share 
              WHERE ParentId = :result.Id AND RowCause = 'Delegate__c'];
        
        System.assertEquals(2, shares.size());
        
        if(!integrationUsers.isEmpty()) {
            System.runAs(integrationUsers.get(0)) {
                result.Order__c = testOrder2.Id;
                update result;
            }
            
            shares = 
                [SELECT Id 
                   FROM GHI_DR_Result_Set__Share 
                  WHERE ParentId = :result.Id AND RowCause = 'Delegate__c'];
            
            System.assertEquals(1, shares.size());  
            
            shares = 
                [SELECT Id 
                   FROM GHI_DR_Result_Set__Share 
                  WHERE ParentId = :result2.Id AND RowCause = 'Delegate__c'];
            
            System.assertEquals(1, shares.size());   
            
            System.runAs(integrationUsers.get(0)) {
                result.Order__c = testOrder.Id;
                update result;
            }     
            
            shares = 
                [SELECT Id 
                   FROM GHI_DR_Result_Set__Share 
                  WHERE ParentId = :result.Id AND RowCause = 'Delegate__c'];
            
            System.assertEquals(2, shares.size());              
        }
        
        delete result;
        delete result2;
        
        System.assertEquals(0, [SELECT count() FROM GHI_DR_Result_Set__Share]);
	}

}