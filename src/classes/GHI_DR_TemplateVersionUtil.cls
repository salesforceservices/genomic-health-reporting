public with sharing class GHI_DR_TemplateVersionUtil {

    public static final String ALL_RO_ERROR = 
        'The template version related to this record is already approved, so this record may not be modified.'; 
    
    private static final String TEMPLATE_RO_ERROR_1 = 
        'This template version is already live and approved, no further edits may be made';
        
    private static final String TEMPLATE_RO_ERROR_2 = 
        'This template version is already approved, only certain fields may be edited.';
    
    private static final Set<String> IGNORE_CHANGE_FIELDS = new Set<String>{
        'active_start_date__c', 'is_template_live__c', 'preview_with__c',
        'preview_with_digital_url__c', 'preview_with_fax_url__c',
        'preview_with_print_url__c'
    };
    
    private static final Set<String> LIVE_CHANGE_FIELDS = new Set<String>{
        'preview_with__c', 'preview_with_digital_url__c', 
        'preview_with_fax_url__c', 'preview_with_print_url__c'
    };    
    
    public static void handleBeforeInsert(List<GHI_DR_Template_Version__c> triggerNew) {
        setVersionNumber(triggerNew);
        GHI_DR_NameUtil.generateTemplateName(triggerNew);
    }
    
    public static void handleBeforeUpdate(List<GHI_DR_Template_Version__c> triggerNew, Map<Id, GHI_DR_Template_Version__c> triggerOld) {
        enforceReadOnly(triggerNew, triggerOld);
        changeTemplateVersionNumber(triggerNew, triggerOld);
        GHI_DR_NameUtil.generateTemplateName(triggerNew);
        copyLanguageToChildren(triggerNew, triggerOld);
    }    

    public static void handleAfterInsert(List<GHI_DR_Template_Version__c> triggerNew, Map<Id, GHI_DR_Template_Version__c> triggerOld) {
        updateTextTemplates(triggerNew, triggerOld);
        populateTemplateVersion(triggerNew);
    }
    
    public static void handleAfterUpdate(List<GHI_DR_Template_Version__c> triggerNew, Map<Id, GHI_DR_Template_Version__c> triggerOld) {
        updateTextTemplates(triggerNew, triggerOld);
        populateTemplateVersion(triggerNew);
    }
    
    private static void populateTemplateVersion(List<GHI_DR_Template_Version__c> templates) {
        List<GHI_DR_Template_Footer_Version__c> footers = new List<GHI_DR_Template_Footer_Version__c>();
        List<GHI_DR_Template_Header_Version__c> headers = new List<GHI_DR_Template_Header_Version__c>();
        
        for(GHI_DR_Template_Version__c template : templates) {
            if(template.Approval_Status__c != 'Approved') {
                footers.add(new GHI_DR_Template_Footer_Version__c(Id = template.GHI_DR_Template_Footer_Version__c));
                headers.add(new GHI_DR_Template_Header_Version__c(Id = template.GHI_DR_Template_Header_Version__c));                
            }
        }
              
        if(!footers.isEmpty()) {
            update footers;
        }
        
        if(!headers.isEmpty()) {
            update headers;
        }

        List<GHI_DR_Drawer_Group_Version__c> groups = new List<GHI_DR_Drawer_Group_Version__c>();
        Set<Id> updated = new Set<Id>();
        
        for(GHI_DR_Template_Header_Version__c header : 
            [SELECT Drawer_Group_1__c, 
                    Drawer_Group_2__c, 
                    Drawer_Group_3__c
               FROM GHI_DR_Template_Header_Version__c
              WHERE Id in :headers]) {
                  
            if(header.Drawer_Group_1__c != null && !updated.contains(header.Drawer_Group_1__c)) {
                updated.add(header.Drawer_Group_1__c);
                groups.add(new GHI_DR_Drawer_Group_Version__c(Id = header.Drawer_Group_1__c));
            }
            
            if(header.Drawer_Group_2__c != null && !updated.contains(header.Drawer_Group_2__c)) {
                updated.add(header.Drawer_Group_2__c);
                groups.add(new GHI_DR_Drawer_Group_Version__c(Id = header.Drawer_Group_2__c));
            }
            
            if(header.Drawer_Group_3__c != null && !updated.contains(header.Drawer_Group_3__c)) {
                updated.add(header.Drawer_Group_3__c);
                groups.add(new GHI_DR_Drawer_Group_Version__c(Id = header.Drawer_Group_3__c));
            } 
        }
              
        if(!groups.isEmpty()) {
            update groups;
        }
    }
    
    public static void changeTemplateVersionNumber(List<GHI_DR_Template_Version__c> templates, Map<Id,GHI_DR_Template_Version__c> old) {
        List<GHI_DR_Template_Version__c> parentRecords = new List<GHI_DR_Template_Version__c>();
        GHI_DR_Template_Version__c oldTV;
        
        for(GHI_DR_Template_Version__c tv : templates) {
            oldTV = old.get(tv.Id);
            
            if(oldTV.Language__c != tv.Language__c) {
                parentRecords.add(tv);
            }
        }     
        
        if(!parentRecords.isEmpty()) {
            setVersionNumber(parentRecords);
        }
    }
    
    public static void copyLanguageToChildren(List<GHI_DR_Template_Version__c> templates, Map<Id,GHI_DR_Template_Version__c> old){
        Set<Id> parents = new Set<Id>();
        Map<Id,String> languageMap = new Map<Id,String>();
        for(GHI_DR_Template_Version__c tv : templates) {
            GHI_DR_Template_Version__c oldTV = old.get(tv.Id);
            if(oldTV.Language__c != tv.Language__c){
                parents.add(tv.Id);
                languageMap.put(tv.Id,tv.Language__c);
            }
        }
        
        if(parents.size() > 0){
            List<GHI_DR_Drawer_Group_Version__c> drawers = 
                [SELECT Id, Template_Version__c, GHI_DR_Drawer_Group__c FROM GHI_DR_Drawer_Group_Version__c WHERE Template_Version__c in :parents];
            List<GHI_DR_Template_Footer_Version__c> footers = 
                [SELECT Id, Template_Version__c, GHI_DR_Template_Footer__c FROM GHI_DR_Template_Footer_Version__c WHERE Template_Version__c in :parents];
            List<GHI_DR_Template_Header_Version__c> headers = 
                [SELECT Id, Template_Version__c, GHI_DR_Template_Header__c FROM GHI_DR_Template_Header_Version__c WHERE Template_Version__c in :parents];
            List<GHI_DR_Text_Version__c> texts = 
                [SELECT Id, Template_Version__c, GHI_DR_Text__c FROM GHI_DR_Text_Version__c WHERE Template_Version__c in :parents];
            List<GHI_DR_Tile_Version__c> tiles = 
                [SELECT Id, GHI_DR_Template_Version__c, GHI_DR_Tile__c FROM GHI_DR_Tile_Version__c WHERE GHI_DR_Template_Version__c in :parents];
            
            for(GHI_DR_Drawer_Group_Version__c drawer : drawers){
                drawer.Language__c = languageMap.get(drawer.Template_Version__c);
            }
            for(GHI_DR_Template_Footer_Version__c footer : footers){
                footer.Language__c = languageMap.get(footer.Template_Version__c);
            }
            for(GHI_DR_Template_Header_Version__c header : headers){
                header.Language__c = languageMap.get(header.Template_Version__c);
            }
            for(GHI_DR_Text_Version__c text : texts){
                text.Language__c = languageMap.get(text.Template_Version__c);
            }
            for(GHI_DR_Tile_Version__c tile : tiles){
                tile.Language__c = languageMap.get(tile.GHI_DR_Template_Version__c);
            }
            
            update GHI_DR_DrawerGroupVersionUtil.setVersionNumber(drawers);
            update GHI_DR_FooterVersionUtil.setVersionNumber(footers);
            update GHI_DR_HeaderVersionUtil.setVersionNumber(headers);
            update GHI_DR_TextVersionUtil.setVersionNumber(texts);
            update GHI_DR_TileVersionUtil.setVersionNumber(tiles);
        }
    }
    
    public static void setVersionNumber(List<GHI_DR_Template_Version__c> templates) {
        Set<Id> parents = new Set<Id>();
        for(GHI_DR_Template_Version__c gv : templates) {
            parents.add(gv.GHI_DR_Template__c);
        }
        
        List<AggregateResult> ars = 
            [SELECT GHI_DR_Template__c pid, MAX(Version_Number__c) num, Language__c lang 
               FROM GHI_DR_Template_Version__c 
              WHERE GHI_DR_Template__c in :parents 
           GROUP BY GHI_DR_Template__c, Language__c];
           
        Map<String, Integer> tvToCounts = new Map<String, Integer>();
        
        for(AggregateResult ar : ars) {
            tvToCounts.put((Id) ar.get('pid') + String.valueOf(ar.get('lang')), Integer.valueOf(ar.get('num')));
        }
        
        Integer currentVersion;
        String key;
        for(GHI_DR_Template_Version__c gv : templates) {
            key = gv.GHI_DR_Template__c + gv.Language__c;
            currentVersion = tvToCounts.containsKey(key) ?
                             tvToCounts.get(key) + 1 : 1;
            
            tvToCounts.put(key, currentVersion);
                             
            gv.Version_Number__c = currentVersion;
        }
    }    
    
    public static void enforceReadOnly(List<GHI_DR_Template_Version__c> templates, Map<Id, GHI_DR_Template_Version__c> old) {
        Set<String> templateFields = Schema.SObjectType.GHI_DR_Template_Version__c.fields.getMap().keySet();
        Set<String> ignoredFields;
        
        for(GHI_DR_Template_Version__c template : templates) {
            if(template.Approval_Status__c == GHI_DR_TemplateUtil.STATUS_APPROVED || 
               old.get(template.Id).Approval_Status__c == GHI_DR_TemplateUtil.STATUS_APPROVED) {
                
                if(old.get(template.Id).Approval_Status__c != GHI_DR_TemplateUtil.STATUS_APPROVED) {
                    continue;
                } else if(template.Is_Template_Live__c && !old.get(template.Id).Is_Template_Live__c) {
                    continue;
                }
                
                ignoredFields = template.Is_Template_Live__c ? LIVE_CHANGE_FIELDS : IGNORE_CHANGE_FIELDS;
                
                for(String templateField : templateFields) {
                    if(!ignoredFields.contains(templateField) && 
                       template.get(templateField) != old.get(template.Id).get(templateField)) {

                        template.addError(template.Is_Template_Live__c ? TEMPLATE_RO_ERROR_1 : TEMPLATE_RO_ERROR_2);
                        break;
                    }
                }
            }
        }
    }    
    
    private static List<GHI_DR_Template_Version__c> updateTextTemplates(List<GHI_DR_Template_Version__c> templates, Map<Id, GHI_DR_Template_Version__c> old) {
        Map<Id, Id> textToTemplate = new Map<Id, Id>();
        List<GHI_DR_Text_Version__c> texts = new List<GHI_DR_Text_Version__c>();
        Id current;
        Id previous;
        
        for(GHI_DR_Template_Version__c template : templates) {
            for(String field : GHI_DR_CloneUtil.TEXTS_TEMPLATE_VERSION) {
                current  = (Id) template.get(field);
                previous = old != null && old.containsKey(template.Id) ?
                           (Id) old.get(template.Id).get(field) : null;
                
                if(current != previous) {
                    if(current == null) {
                        textToTemplate.put(previous, null);
                    } else {
                        textToTemplate.put(current, template.Id);
                    }
                }
            }
        }

        GHI_DR_TextVersionUtil.updateTextTemplates(textToTemplate); 
        return templates;
    }
    
}