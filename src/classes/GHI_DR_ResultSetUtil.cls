public with sharing class GHI_DR_ResultSetUtil {

    private static final String UPDATE_ERROR_NON_INTEGRATION =
        'Only the integration user is allowed to update non-test result sets.';

    private static final String UPDATE_ERROR_TEST_SWITCH = 
        'The \'Is Test\' field cannot change on update.';

    public static void handleBeforeInsert(List<GHI_DR_Result_Set__c> triggerNew) {
        GHI_DR_Distribution.linkDistributionEvent(triggerNew);
        GHI_DR_TemplateUtil.setDigitalTemplate(triggerNew);
    }
    
    public static void handleBeforeDelete(List<GHI_DR_Result_Set__c> triggerNew) {
        removeDelegateShares(triggerNew);
    }    
    
    public static void handleAfterInsert(List<GHI_DR_Result_Set__c> triggerNew) {
        addDelegateShares(triggerNew);
        GHI_DR_Distribution.distributeResultSet(triggerNew);
    }
    
    public static void handleAfterUpdate(List<GHI_DR_Result_Set__c> triggerNew, Map<Id, GHI_DR_Result_Set__c> oldMap) {
        updateDelegateShares(triggerNew, oldMap);
    }    
    
    public static void handleBeforeUpdate(List<GHI_DR_Result_Set__c> triggerNew, Map<Id, GHI_DR_Result_Set__c> oldMap) {
        preventIsTestUpdate(triggerNew, oldMap);
        preventUpdate(triggerNew);
        GHI_DR_TemplateUtil.setDigitalTemplate(triggerNew);
        GHI_DR_Distribution.distributeResultSet(triggerNew);
    }
    
    private static void addDelegateShares(List<GHI_DR_Result_Set__c> results) {
        List<GHI_DR_DelegateUtil.Delegate> shareables = getSharableDelegates(results);
        shareables = GHI_DR_DelegateUtil.filterSharedDelegates(shareables);
        GHI_DR_DelegateUtil.shareDelegates(shareables);
    }
    
    private static void removeDelegateShares(List<GHI_DR_Result_Set__c> results) {
        List<GHI_DR_DelegateUtil.Delegate> shareables = getSharableDelegates(results);
        GHI_DR_DelegateUtil.removeSharedDelegates(shareables);
    }
    
    private static List<GHI_DR_DelegateUtil.Delegate> getSharableDelegates(List<GHI_DR_Result_Set__c> results) {
        List<GHI_DR_DelegateUtil.Delegate> shareables = new List<GHI_DR_DelegateUtil.Delegate>();
        
        Set<Id> orderIds = new Set<Id>();
        Map<Id, Id> resultToOrder = new Map<Id, Id>();
        for(GHI_DR_Result_Set__c result : results) {
            if(String.isNotBlank(result.Order__c)) {
                orderIds.add(result.Order__c);
                resultToOrder.put(result.Id, result.Order__c);
            }
        }
        
        Set<Id> hcpIds = new Set<Id>();
        Map<Id, Order> orders = new Map<Id, Order>(
            [SELECT Id, CreatedById FROM Order WHERE Id in :orderIds]);
        
        for(Order ord : orders.values()) {
            hcpIds.add(ord.CreatedById);
        }
        
        Map<Id, Id> hcpUserToContact = new Map<Id, Id>();
        List<User> hcpUsers = 
            [SELECT Id, ContactId
               FROM User 
              WHERE Id in :hcpIds AND ContactId <> null];
              
        for(User hcpUser : hcpUsers) {
            hcpUserToContact.put(hcpUser.Id, hcpUser.ContactId);
        }
        
        Map<Id, Set<Id>> hcpToSponsor = new Map<Id, Set<Id>>();
        List<OSM_Delegate__c> delegates = 
            [SELECT OSM_Contact__c, OSM_HCP__c 
               FROM OSM_Delegate__c 
              WHERE OSM_HCP__c in :hcpUserToContact.values() AND OSM_Contact__c <> null];      
        
        Set<Id> sponsorIds = new Set<Id>();
        for(OSM_Delegate__c delegate : delegates) {
            if(!hcpToSponsor.containsKey(delegate.OSM_HCP__c)) {
                hcpToSponsor.put(delegate.OSM_HCP__c, new Set<Id>());
            }
            
            sponsorIds.add(delegate.OSM_Contact__c);
            hcpToSponsor.get(delegate.OSM_HCP__c).add(delegate.OSM_Contact__c);
        }

        Map<Id, Id> sponsorToUser = new Map<Id, Id>();
        List<User> sponsors = 
            [SELECT Id, ContactId 
               FROM User
              WHERE ContactId in :sponsorIds];
              
        for(User sponsor : sponsors) {
            sponsorToUser.put(sponsor.ContactId, sponsor.Id);
        }
     
        Id delegateId;
        Id delegateContact;
        Id sponsorId;
        for(GHI_DR_Result_Set__c result : results) {
            if(String.isBlank(result.Order__c)) {
                continue;
            }
            
            delegateId      = orders.get(result.Order__c).CreatedById;
            delegateContact = hcpUserToContact.get(delegateId);

            shareables.add(new GHI_DR_DelegateUtil.Delegate(
                delegateId, delegateContact, result.Order__c, result.Id
            ));
            
            if(hcpToSponsor.containsKey(delegateContact)) {
                for(Id sponsorContact : hcpToSponsor.get(delegateContact)) {
                    sponsorId = sponsorToUser.get(sponsorContact);
                    
                    shareables.add(new GHI_DR_DelegateUtil.Delegate(
                        sponsorId, sponsorContact, result.Order__c, result.Id
                    ));                    
                }                
            }
        }

        return shareables;
    }
    
    private static void updateDelegateShares(List<GHI_DR_Result_Set__c> results, Map<Id, GHI_DR_Result_Set__c> old) {
        removeDelegateShares(old.values());
        addDelegateShares(results);
    }
    
    private static void preventIsTestUpdate(List<GHI_DR_Result_Set__c> triggerNew, Map<Id, GHI_DR_Result_Set__c> oldMap) {
        for(GHI_DR_Result_Set__c result : triggerNew) {
            if(result.Is_Test__c != oldMap.get(result.Id).Is_Test__c) {
                result.addError(UPDATE_ERROR_TEST_SWITCH);
            }
        }
    }
    
    private static void preventUpdate(List<GHI_DR_Result_Set__c> triggerNew) {
        List<User> currentUser =
            [SELECT GHI_DR_Integration_User__c
               FROM User 
              WHERE Id = :UserInfo.getUserId()];
        
        if(!currentUser.isEmpty() && !currentUser.get(0).GHI_DR_Integration_User__c) {
            for(GHI_DR_Result_Set__c result : triggerNew) {
                if(!result.Is_Test__c) {
                    result.addError(UPDATE_ERROR_NON_INTEGRATION);
                }
            }
        }
        
    }

}