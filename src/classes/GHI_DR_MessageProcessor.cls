public with sharing class GHI_DR_MessageProcessor {

    private static final String MSG_TYPE_PROSTATE = 'Prostate';
    private static final String ER_NEGATIVE       = 'Node_Negative';
    private static final String MSG_TYPE_BREAST   = 'IBC';
    private static final String RESULT_SET_OBJECT = 'GHI_DR_Result_Set__c';

    public static String processResultsDataMessage(ESBInboundMessage__c msg) {
        DOM.Document doc = getDocumentFromMessage(msg);
        String msgType = getMessageType(doc);
        
        if(msgType == MSG_TYPE_PROSTATE) {
            return String.valueOf(createProstateResultSet(doc));
        } else if(msgType == MSG_TYPE_BREAST + ER_NEGATIVE) {
            return String.valueOf(createBreastResultSet(doc));
        }
        
        return 'false';
    }
    
    private static DOM.Document getDocumentFromMessage(ESBInboundMessage__c msg) {
        DOM.Document doc             = new DOM.Document();
        Record_Type__c systemSetting = Record_Type__c.getOrgDefaults();
        
        String xmlMessage = OSM_Messaging.returnBase64EncodeDecode(
            msg.ESBMessage__c, 
            systemSetting, 
            false);
        
        doc.load(xmlMessage);  
        
        return doc;
    }
    
    private static Boolean createProstateResultSet(DOM.Document doc) {
        GHI_DR_Result_Set__c result = new GHI_DR_Result_Set__c();
        result.RecordTypeId = getRecordTypeId(MSG_TYPE_PROSTATE);
        DOM.XMLNode root = doc.getRootElement();

        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'OrderLineItem');
        node = getChildElement(node, 'ResultSet');
        String gleasonQualifier = getChildElementTextByAttribute(node, 'Result', 'name', 'ShowGleasonScoreQualifier');
        
        if(String.isNotBlank(gleasonQualifier) && gleasonQualifier.toLowerCase() == 'true') {
            return false;
        }
        
        setProstateData(root, result);
        setProstateTestCriteriaData(root, result);
        setCommonData(root, result);
        
        try {
            insert result;
            return true;
        } catch(Exception ex) {
            return false;
        }
        
        return false;
    }
    
    private static Boolean createBreastResultSet(DOM.Document doc) {
        GHI_DR_Result_Set__c result = new GHI_DR_Result_Set__c();
        result.RecordTypeId = getRecordTypeId(MSG_TYPE_BREAST);
        DOM.XMLNode root = doc.getRootElement();

        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'OrderLineItem');
        node = getChildElement(node, 'ResultSet');
        String erPositive = getChildElementTextByAttribute(node, 'Result', 'name', 'ERPositive');
        
        if(String.isNotBlank(erPositive) && erPositive.toLowerCase() == 'false') {
            return false;
        }

        setBreastData(root, result);
        setBreastTestCriteriaData(root, result);
        setAccountData(root, result);
        setCommonData(root, result);
        
        try {
            insert result;
            return true;
        } catch(Exception ex) {
            return false;
        }
        
        return false;
    }
    
    private static void setCommonData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        setComments(root, result);
        setDistributionEvent(root, result);
        setSpecimenData(root, result);
        setOLIData(root, result);  
        setOrderData(root, result);
        setReportData(root, result);
        setPatientData(root, result);
        setPhysicianName(root, result);
        setAdditionalRecipientName(root, result); 
    }    
    
    private static void setComments(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'OrderLineItem');

        result.Comments__c = getChildElementText(node, 'Comments');
        result.Has_Comments__c = String.isNotBlank(result.Comments__c);
    }
    
    private static void setDistributionEvent(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        String reportId = getChildElementText(root, 'ReportId');
        String distributedId = getChildElementText(root, 'DistributedId');
        
        if(String.isNotBlank(reportId)) {
            result.Distribution_Event_Report_Id__c = reportId;
        }
        
        if(String.isNotBlank(distributedId)) {
            result.Distributed_ID__c = distributedId;
        }
    }
    
    private static void setProstateData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'OrderLineItem');
        node = getChildElement(node, 'ResultSet');
        
        DOM.XMLNode gps = getChildElementByAttribute(node, 'Result', 'name', 'GPScore');
        if(gps != null) {
            result.GPS_Low_Range__c = Integer.valueOf(getAttributeValue(gps, 'lowRange'));
            result.GPS_High_Range__c = Integer.valueOf(getAttributeValue(gps, 'highRange'));
            result.GPS_Unrounded__c = Decimal.valueOf(getAttributeValue(gps, 'unrounded'));
            result.GPS_Value__c = Integer.valueOf(gps.getText());
            result.Patient_Summary_Value__c = result.GPS_Value__c;
        }
        
        DOM.XMLNode gpsRange = getChildElementByAttribute(node, 'Result', 'name', 'GPScoreInRange');
        if(gpsRange != null) {
            result.GPS_In_Range__c = Boolean.valueOf(gpsRange.getText());
        }
        
        DOM.XMLNode riskGroup = getChildElementByAttribute(node, 'Result', 'name', 'RiskGroup');
        if(riskGroup != null) {
            result.NCCN_Category__c = Integer.valueOf(riskGroup.getText()) - 1;
        }
    }

    private static void setProstateTestCriteriaData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'OrderLineItem');
        node = getChildElement(node, 'TestCriteriaGroup');
        
        if(node != null) {
            
            
            String psa = getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'ProstateDRPSA');
            String psaDensity = getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'ProstateDRPSADensity');
            String fourPlusThree = getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'ProstateDR4Plus3Cores');
            
            if(String.isNotBlank(psa)) {
                result.PSA__c = Decimal.valueOf(psa);
            }
            
            if(String.isNotBlank(psaDensity)) {
                result.PSA_Density__c = Decimal.valueOf(psaDensity);
            }
            
            if(String.isNotBlank(fourPlusThree)) {
                result.Number_4_Plus_3_Cores__c = Integer.valueOf(fourPlusThree);
            }
            //System.assertEquals('wat', 'somethingelse', result.Number_4_Plus_3_Cores__c);
                    
            result.Prostate_Volume__c = 
                getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'ProstateDRVolume');
                
            result.Clinical_Stage__c = 
                getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'ProstateDRClinicalStage');
                
            result.Gleason_Score__c = 
                getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'ProstateDRGleasonScore');
                
            result.Max_tumor_involvement__c = 
                getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'ProstateDRMaxTumorInvolvement');
            
            String positiveCores = getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'ProstateDRPositiveCores');
            String collectedCores = getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'ProstateDRCoresCollected');
            
            if(String.isBlank(positiveCores)) {
                positiveCores = '';
            }
            
            if(String.isBlank(collectedCores)) {
                collectedCores = '';
            }
            if(result.Gleason_Score__c == '4+3'){
                System.debug('result.Number_4_Plus_3_Cores__c'+ result.Number_4_Plus_3_Cores__c);
                if(result.Number_4_Plus_3_Cores__c == 1){
                    result.Number_cores_positive_collected__c = positiveCores + ' / ' + collectedCores + ' [1 - 4+3 positive]';
                }else if (result.Number_4_Plus_3_Cores__c > 1){
                    result.Number_cores_positive_collected__c = positiveCores + ' / ' + collectedCores + ' [2 - 4+3 positive]';
                }
                
            }else{
                result.Number_cores_positive_collected__c = positiveCores + ' / ' + collectedCores;
            }
             
             
        }
    }
    
    private static void setBreastTestCriteriaData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'OrderLineItem');
        node = getChildElement(node, 'TestCriteriaGroup');
        
        if(node != null) {
            result.Clinical_Stage__c = getChildElementTextByAttribute(node, 'TestCriteria', 'Name', 'IBCClinicalStage');
        }
    }
    
    private static void setBreastData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'OrderLineItem');
        node = getChildElement(node, 'ResultSet');
        
        DOM.XMLNode recurrence = getChildElementByAttribute(node, 'Result', 'name', 'Recurrence');
        if(recurrence != null) { 
            result.Low_Range__c = Decimal.valueOf(getAttributeValue(recurrence, 'lowRange'));
            result.High_Range__c = Decimal.valueOf(getAttributeValue(recurrence, 'highRange'));
            result.Recurrence__c = Integer.valueOf(recurrence.getText());
            result.Recurrence_Unrounded__c = Decimal.valueOf(getAttributeValue(recurrence, 'unrounded'));
            result.Absolute_Chemotherapy_Benefit__c = result.Recurrence__c;
            result.Patient_Summary_Value__c = result.Recurrence__c;
            result.B20_Value__c = result.Recurrence__c;
            result.B14_Value__c = result.Recurrence__c;
        }
        
        DOM.XMLNode er = getChildElementByAttribute(node, 'CompositeResult', 'name', 'ER');
        if(er != null) {
            
            DOM.XMLNode geneValue = getChildElementByAttribute(er, 'Result', 'name', 'GeneValue');
            if(geneValue != null) {
                result.ER_Gene_Value__c = Decimal.valueOf(geneValue.getText());
                result.ER_Gene_Value_Unrounded__c = Decimal.valueOf(getAttributeValue(geneValue, 'unrounded'));
                result.ER_Gene_Value_High_Range__c = Decimal.valueOf(getAttributeValue(geneValue, 'highRange'));
                result.ER_Gene_Value_Low_Range__c = Decimal.valueOf(getAttributeValue(geneValue, 'lowRange'));
            }
            
            DOM.XMLNode classification = getChildElementByAttribute(er, 'Result', 'name', 'Classification');
            if(classification != null) {
                result.ER_Classification__c = classification.getText();
            }
            
            DOM.XMLNode range = getChildElementByAttribute(er, 'Result', 'name', 'Range');
            if(range != null) {
                result.ER_Range__c = range.getText();
            }            
        }
        
        DOM.XMLNode pr = getChildElementByAttribute(node, 'CompositeResult', 'name', 'PR');
        if(pr != null) {
            
            DOM.XMLNode geneValue = getChildElementByAttribute(pr, 'Result', 'name', 'GeneValue');
            if(geneValue != null) {
                result.PR_Gene_Value__c = Decimal.valueOf(geneValue.getText());
                result.PR_Gene_Value_Unrounded__c = Decimal.valueOf(getAttributeValue(geneValue, 'unrounded'));
                result.PR_Gene_Value_High_Range__c = Decimal.valueOf(getAttributeValue(geneValue, 'highRange'));
                result.PR_Gene_Value_Low_Range__c = Decimal.valueOf(getAttributeValue(geneValue, 'lowRange'));
            }
            
            DOM.XMLNode classification = getChildElementByAttribute(pr, 'Result', 'name', 'Classification');
            if(classification != null) {
                result.PR_Classification__c = classification.getText();
            }
            
            DOM.XMLNode range = getChildElementByAttribute(pr, 'Result', 'name', 'Range');
            if(range != null) {
                result.PR_Range__c = range.getText();
            }            
        }
        
        DOM.XMLNode her2 = getChildElementByAttribute(node, 'CompositeResult', 'name', 'HER2');
        if(her2 != null) {
            
            DOM.XMLNode geneValue = getChildElementByAttribute(her2, 'Result', 'name', 'GeneValue');
            if(geneValue != null) {
                result.HER2_Gene_Value__c = Decimal.valueOf(geneValue.getText());
                result.HER2_Gene_Value_Unrounded__c = Decimal.valueOf(getAttributeValue(geneValue, 'unrounded'));
                result.HER2_Gene_Value_High_Range__c = Decimal.valueOf(getAttributeValue(geneValue, 'highRange'));
                result.HER2_Gene_Value_Low_Range__c = Decimal.valueOf(getAttributeValue(geneValue, 'lowRange'));
            }
            
            DOM.XMLNode classification = getChildElementByAttribute(her2, 'Result', 'name', 'Classification');
            if(classification != null) {
                result.HER2_Classification__c = classification.getText();
            }
            
            DOM.XMLNode range = getChildElementByAttribute(her2, 'Result', 'name', 'Range');
            if(range != null) {
                result.HER2_Range__c = range.getText();
            }            
        }
        
        DOM.XMLNode b14 = getChildElementByAttribute(node, 'CompositeResult', 'name', 'NodeNegB14');
        if(b14 != null) {
            
            DOM.XMLNode defined = getChildElementByAttribute(b14, 'Result', 'name', 'Defined');
            if(defined != null) {
                result.B14_Defined__c = Boolean.valueOf(defined.getText());
            }
            
            DOM.XMLNode rate = getChildElementByAttribute(b14, 'Result', 'name', 'Rate');
            if(rate != null) {
                result.B14_Rate_Value__c = Integer.valueOf(rate.getText());
                result.B14_Rate_Unrounded__c = Decimal.valueOf(getAttributeValue(rate, 'unrounded'));
                result.B14_Rate_High_Range__c = Decimal.valueOf(getAttributeValue(rate, 'highRange'));
                result.B14_Rate_Low_Range__c = Decimal.valueOf(getAttributeValue(rate, 'lowRange'));  
                result.B14_Rate_Units__c = getAttributeValue(rate, 'units');
            }
            
            DOM.XMLNode lower = getChildElementByAttribute(b14, 'Result', 'name', 'LowerConfidence');
            if(lower != null) {
                result.B14_LC_Value__c = Integer.valueOf(lower.getText());
                result.B14_LC_Unrounded__c = Decimal.valueOf(getAttributeValue(lower, 'unrounded'));
                result.B14_LC_High_Range__c = Decimal.valueOf(getAttributeValue(lower, 'highRange'));
                result.B14_LC_Low_Range__c = Decimal.valueOf(getAttributeValue(lower, 'lowRange'));  
                result.B14_LC_Units__c = getAttributeValue(lower, 'units');               
            } 
            
            DOM.XMLNode upper = getChildElementByAttribute(b14, 'Result', 'name', 'UpperConfidence');
            if(upper != null) {
                result.B14_UC_Value__c = Integer.valueOf(upper.getText());
                result.B14_UC_Unrounded__c = Decimal.valueOf(getAttributeValue(upper, 'unrounded'));
                result.B14_UC_High_Range__c = Decimal.valueOf(getAttributeValue(upper, 'highRange'));
                result.B14_UC_Low_Range__c = Decimal.valueOf(getAttributeValue(upper, 'lowRange'));  
                result.B14_UC_Units__c = getAttributeValue(upper, 'units');                  
            }             
        }
    }

    private static void setSpecimenData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'OrderLineItem');
        node = getChildElement(node, 'Specimen');
        
        if(node != null) {
            result.Specimen_Type_ID__c = getChildElementText(node, 'SpecimenID');
            result.Specimen_Received__c = toDate(getChildElementText(node, 'RecvDate'));
            result.Date_of_Collection__c = toDateMDY(getChildElementText(node, 'CollectDate'));
            result.NCCN_Risk_Group__c = getChildElementText(node, 'NCCNRiskCategory');
        }
    }

    private static void setOLIData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'OrderLineItem');
        
        if(node != null) {
            result.Lab_Directors__c = getChildElementText(node, 'LabDirectors');
            
            String oliNumber = getAttributeValue(node, 'orderLineItemNumber');
            if(String.isNotBlank(oliNumber)) {
                result.OLI_Number__c = oliNumber;
                
                //Clinical_Stage__c
                //Gleason_Score__c
                //Number_cores_positive_collected__c
            }
        }
    }

    private static void setAccountData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'Account');
        
        if(node != null) {
            result.Client__c = getChildElementText(node, 'AcctName');
        }
    }

    private static void setOrderData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        
        if(node != null) {
            result.Medical_Record_Patient__c = getChildElementText(node, 'MedicalRecordNumber');
            result.Study__c = getChildElementText(node, 'StudyName');
            result.Order_Number__c = getChildElementText(node, 'OrderNumber');
            
            if(String.isNotBlank(result.Order_Number__c)) {
                List<Order> orders = 
                    [SELECT Id 
                       FROM Order
                      WHERE OrderNumber = :result.Order_Number__c
                      LIMIT 1];
                      
                if(!orders.isEmpty()) {
                    result.Order__c = orders.get(0).Id;
                }
            }
        }
    }

    private static void setReportData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        
        if(node != null) {
            result.Report_Date__c = toDate(getChildElementText(node, 'ReportDate'));
            result.Report_Number__c = getChildElementText(node, 'ReportId');
        }
    }
    
    private static void setPatientData(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElement(node, 'Order');
        node = getChildElement(node, 'Patient');
        
        if(node != null) {
            DOM.XMLNode nameNode = getChildElement(node, 'Name');
            
            result.Patient_Name__c = 
                getChildElementText(nameNode, 'FirstName') + ' ' +
                getChildElementText(nameNode, 'LastName');
            
            result.Patient_First_Name__c = getChildElementText(nameNode, 'FirstName');
            result.Patient_Last_Name__c = getChildElementText(nameNode, 'LastName');
            result.Patient_Gender__c = getChildElementText(node, 'Gender');
            result.Patient_Birthdate__c = toDate(getChildElementText(node, 'DOB'));
        }
    }
    
    private static void setPhysicianName(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        result.Ordering_Physician__c = getRecipientName(root, result, 'Physician');
    }   
    
    private static void setAdditionalRecipientName(DOM.XMLNode root, GHI_DR_Result_Set__c result) {
        result.Additional_Recipient__c = getRecipientName(root, result, 'Other');
    }       
    
    private static String getRecipientName(DOM.XMLNode root, GHI_DR_Result_Set__c result, String recipientType) {
        DOM.XMLNode node = getChildElement(root, 'Report');
        node = getChildElementByAttribute(node, 'Recipients', 'role', recipientType);
        node = getChildElement(node, 'Name');
        
        String first  = getChildElementText(node, 'FirstName');
        String middle = getChildElementText(node, 'MiddleName');
        String last   = getChildElementText(node, 'LastName');
        
        first  = String.isBlank(first) ? '' : first;
        middle = String.isBlank(middle) ? '' : middle;
        last   = String.isBlank(last) ? '' : last;
        
        return String.join(new List<String>{first, middle, last}, ' ');        
    }

    private static String getMessageType(DOM.Document doc) {
        DOM.XMLNode node = doc.getRootElement();
        
        node = getChildElement(node, 'Report');
        node = getChildElement(node, 'Order');
        
        String msgType = getChildElementText(node, 'OrderableName');
        
        if(msgType == MSG_TYPE_BREAST) {
            node = getChildElement(node, 'OrderLineItem');
            node = getChildElement(node, 'Specimen');
            
            String erNodalStatus = getChildElementText(node, 'NodalStatus');
            
            if(String.isNotBlank(erNodalStatus)) {
                msgType += erNodalStatus;
            }
        }
        
        return msgType;
    }

    private static Date toDateMDY(String dateValue) {
        if(String.isNotBlank(dateValue) && dateValue.contains('-')) {
            List<String> dateSplit = dateValue.split('-');
            
            return Date.newInstance(
                Integer.valueOf(dateSplit.get(2)), 
                Integer.valueOf(dateSplit.get(0)), 
                Integer.valueOf(dateSplit.get(1)));
        }
        
        return null;
    }
    
    private static Date toDate(String dateValue) {
        if(String.isNotBlank(dateValue) && dateValue.contains('-')) {
            List<String> dateSplit = dateValue.split('-');
            
            return Date.newInstance(
                Integer.valueOf(dateSplit.get(0)), 
                Integer.valueOf(dateSplit.get(1)),
                Integer.valueOf(dateSplit.get(2)));
        }
        
        return null;
    }
    
    private static Id getRecordTypeId(String developerName) {
        List<RecordType> recordTypes =
            [SELECT Id 
               FROM RecordType 
              WHERE SobjectType = :RESULT_SET_OBJECT AND 
                    DeveloperName = :developerName 
              LIMIT 1];
        
        if(!recordTypes.isEmpty()) {
            return recordTypes.get(0).Id;
        }
        
        return null;
    }

    private static String getChildElementTextByAttribute(DOM.XMLNode node, String elementName, String attributeName, String attributeValue) {
        if(node != null) {
            List<DOM.XMLNode> children = node.getChildElements();
            
            for(DOM.XMLNode child : children) {
                if(child.getNodeType() == DOM.XMLNodeType.ELEMENT && 
                   child.getName() == elementName &&
                   getAttributeValue(child, attributeName) == attributeValue) {
                       
                    return child.getText();
                }
            }
        }
        
        return null;        
    }
    
    private static DOM.XMLNode getChildElementByAttribute(DOM.XMLNode node, String elementName, String attributeName, String attributeValue) {
        if(node != null) {
            List<DOM.XMLNode> children = node.getChildElements();
            
            for(DOM.XMLNode child : children) {
                if(child.getNodeType() == DOM.XMLNodeType.ELEMENT && 
                   child.getName() == elementName &&
                   getAttributeValue(child, attributeName) == attributeValue) {
                       
                    return child;
                }
            }
        }
        
        return null;        
    }

    private static String getAttributeValue(DOM.XMLNode node, String attributeName) {
        if(node != null) {
            return node.getAttributeValue(attributeName, node.getNamespace());
        }
        
        return null;
    }
    
    private static DOM.XMLNode getChildElement(DOM.XMLNode node, String elementName) {
        if(node != null) {
            List<DOM.XMLNode> children = node.getChildElements();
            
            for(DOM.XMLNode child : children) {
                if(child.getNodeType() == DOM.XMLNodeType.ELEMENT && child.getName() == elementName) {
                    return child;
                }
            }
        }
        
        return null;
    }
    
    private static String getChildElementText(DOM.XMLNode node, String elementName) {
        if(node != null) {
            List<DOM.XMLNode> children = node.getChildElements();
            
            for(DOM.XMLNode child : children) {
                if(child.getNodeType() == DOM.XMLNodeType.ELEMENT && child.getName() == elementName) {
                    return child.getText();
                }
            }
        }
        
        return null;        
    }        

}