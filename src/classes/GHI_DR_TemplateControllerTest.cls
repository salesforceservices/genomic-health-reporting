@isTest
private class GHI_DR_TemplateControllerTest {

	private static testMethod void testConstructorByResult() {
	    Test.startTest();
	    Contact con = GHI_DR_TestUtil.createPatient('Joe', 'Smith');
	    GHI_DR_Result_Set__c resultSet = GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, true);
	    
	    
	    Account acct = GHI_DR_TestUtil.createAccount('acct1');
	    Order order1 = GHI_DR_TestUtil.createOrder('testOrder', GHI_DR_TestUtil.RESULT_TYPE_BREAST, acct.Id);
	    
	    GHI_DR_Template_Header__c testHeader = GHI_DR_TestUtil.createHeader('test-header');
	    GHI_DR_Template_Footer__c testFooter = GHI_DR_TestUtil.createFooter('test-footer');
	    GHI_DR_Template__c testTemplate = GHI_DR_TestUtil.createTemplate('test-template');
	    
	    GHI_DR_Template_Header_Version__c testHeaderVersion = 
	        GHI_DR_TestUtil.createHeaderVersion('thv', 'thvComponent', testHeader.Id);
	        
	    GHI_DR_Template_Footer_Version__c testFooterVersion = 
	        GHI_DR_TestUtil.createFooterVersion('tfv', 'tfvComponent', testFooter.Id);
	        
	    GHI_DR_Template_Version__c testTemplateVersion = 
	        GHI_DR_TestUtil.createTemplateVersion('ttv', testTemplate.Id, testHeaderVersion.Id, testFooterVersion.Id);
	        
	    testTemplateVersion.Approval_Status__c = 'Approved';
	    update testTemplateVersion;
	    
	    resultSet.Digital_Template__c = testTemplateVersion.Id;
	    update resultSet;
	    
	    Test.stopTest();
	    
	    Test.setCurrentPage(new PageReference('/apex/GHI_DR_WebTemplate?rid=' + resultSet.Id));
	    
        GHI_DR_TemplateController ctrl = new GHI_DR_TemplateController();
        System.assertEquals(testTemplateVersion.Id, ctrl.templateId);
	}	
	
	private static testMethod void testGetTemplateInfoNull() {
        GHI_DR_TemplateInfo info = GHI_DR_TemplateController.getTemplate(null, null);
        
        System.assertEquals(null, info.footer);
        System.assertEquals(null, info.header);
        System.assertEquals(null, info.template);
        System.assertEquals(null, info.tiles);
	}
	
	private static testMethod void testGetTemplateInfo() {
	    Test.startTest();
	    
	    GHI_DR_Template_Header__c testHeader = GHI_DR_TestUtil.createHeader('test-header');
	    GHI_DR_Template_Footer__c testFooter = GHI_DR_TestUtil.createFooter('test-footer');
	    GHI_DR_Template__c testTemplate = GHI_DR_TestUtil.createTemplate('test-template');
	    
	    GHI_DR_Template_Header_Version__c testHeaderVersion = 
	        GHI_DR_TestUtil.createHeaderVersion('thv', 'thvComponent', testHeader.Id);
	        
	    GHI_DR_Template_Footer_Version__c testFooterVersion = 
	        GHI_DR_TestUtil.createFooterVersion('tfv', 'tfvComponent', testFooter.Id);
	        
	    GHI_DR_Template_Version__c testTemplateVersion =
	        GHI_DR_TestUtil.createTemplateVersion('ttv', testTemplate.Id, testHeaderVersion.Id, testFooterVersion.Id);

        GHI_DR_Drawer_Group__c drawerGroup = GHI_DR_TestUtil.createDrawerGroup('tdg');
        GHI_DR_Drawer_Group_Version__c groupVersion = GHI_DR_TestUtil.createDrawerGroupVersion('dgv', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem1 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', GHI_DR_TemplateInfo.USE_STATIC, groupVersion.Id);
        GHI_DR_Drawer_Item__c drawerItem2 = GHI_DR_TestUtil.createDrawerItem('label2', 'value', GHI_DR_TemplateInfo.USE_STATIC, groupVersion.Id);

        GHI_DR_Drawer_Group_Version__c groupVersion2 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv2', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem3 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', GHI_DR_TemplateInfo.USE_STATIC, groupVersion2.Id);
        
        GHI_DR_Drawer_Group_Version__c groupVersion3 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv3', drawerGroup.Id);        
        
        testHeaderVersion.Drawer_Group_1__c = groupVersion.Id;
        testHeaderVersion.Drawer_Group_2__c = groupVersion2.Id;
        testHeaderVersion.Drawer_Group_3__c = groupVersion3.Id;
        update testHeaderVersion;
        
        GHI_DR_Tile__c testTile1 = GHI_DR_TestUtil.createTile('test tile 1');
        
        GHI_DR_Tile__c testTile2 = GHI_DR_TestUtil.createTile('test tile 2');
        
        GHI_DR_Tile_Version__c tv1 = GHI_DR_TestUtil.createTileVersion('tv1', 'TileComponentName1', testTile1.Id, testTemplateVersion.Id);
        GHI_DR_Tile_Version__c tv2 = GHI_DR_TestUtil.createTileVersion('tv2', 'TileComponentName2', testTile2.Id, testTemplateVersion.Id);
	    
	    Test.stopTest();
	    
        GHI_DR_TemplateInfo info = GHI_DR_TemplateController.getTemplate(null, testTemplateVersion.Id);
        
        System.assertEquals(testFooterVersion.Id, info.footer.Id);
        System.assertEquals(testHeaderVersion.Id, info.header.Id);
        System.assertEquals(testTemplateVersion.Id, info.template.Id);
        System.assertEquals(2, info.tiles.size());
        System.assertEquals(2, info.drawerGroup1.size());
        System.assertEquals(1, info.drawerGroup2.size());
        System.assertEquals(0, info.drawerGroup3.size());
	}	
	
	private static testMethod void testHeaderTexts() {
	    Test.startTest();
	    
	    GHI_DR_Template_Header__c testHeader = GHI_DR_TestUtil.createHeader('test-header');
	    GHI_DR_Template_Footer__c testFooter = GHI_DR_TestUtil.createFooter('test-footer');
	    GHI_DR_Template__c testTemplate = GHI_DR_TestUtil.createTemplate('test-template');
	    
	    GHI_DR_Template_Header_Version__c testHeaderVersion = 
	        GHI_DR_TestUtil.createHeaderVersion('thv', 'thvComponent', testHeader.Id);
	        
	    GHI_DR_Template_Footer_Version__c testFooterVersion = 
	        GHI_DR_TestUtil.createFooterVersion('tfv', 'tfvComponent', testFooter.Id);
	        
	    GHI_DR_Template_Version__c testTemplateVersion =
	        GHI_DR_TestUtil.createTemplateVersion('ttv', testTemplate.Id, testHeaderVersion.Id, testFooterVersion.Id);

        GHI_DR_Drawer_Group__c drawerGroup = GHI_DR_TestUtil.createDrawerGroup('tdg');
        GHI_DR_Drawer_Group_Version__c groupVersion = GHI_DR_TestUtil.createDrawerGroupVersion('dgv', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem1 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', GHI_DR_TemplateInfo.USE_STATIC, groupVersion.Id);
        GHI_DR_Drawer_Item__c drawerItem2 = GHI_DR_TestUtil.createDrawerItem('label2', 'value', GHI_DR_TemplateInfo.USE_STATIC, groupVersion.Id);

        GHI_DR_Drawer_Group_Version__c groupVersion2 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv2', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem3 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', GHI_DR_TemplateInfo.USE_STATIC, groupVersion2.Id);
        
        GHI_DR_Drawer_Group_Version__c groupVersion3 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv3', drawerGroup.Id);        
        
        testHeaderVersion.Drawer_Group_1__c = groupVersion.Id;
        testHeaderVersion.Drawer_Group_2__c = groupVersion2.Id;
        testHeaderVersion.Drawer_Group_3__c = groupVersion3.Id;
        update testHeaderVersion;
        
        GHI_DR_Tile__c testTile1 = GHI_DR_TestUtil.createTile('test tile 1');
        
        GHI_DR_Tile__c testTile2 = GHI_DR_TestUtil.createTile('test tile 2');
        
        GHI_DR_Tile_Version__c tv1 = GHI_DR_TestUtil.createTileVersion('tv1', 'TileComponentName1', testTile1.Id, testTemplateVersion.Id);
        GHI_DR_Tile_Version__c tv2 = GHI_DR_TestUtil.createTileVersion('tv2', 'TileComponentName2', testTile2.Id, testTemplateVersion.Id);
	    
	    GHI_DR_Text__c text1 = GHI_DR_TestUtil.createText('testText1');
	    GHI_DR_Text_Version__c textV1 = GHI_DR_TestUtil.createTextVersion('testText1 - Eng - 1', 'some cool {{Recurrence__c}} text', text1.Id);
	    
        testHeaderVersion.Text_1__c = textV1.Id;
	    testHeaderVersion.Text_3__c = textV1.Id;
	    update testHeaderVersion;
	    
	    GHI_DR_Result_Set__c rs = GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, true);
	    rs.Recurrence__c = 10;
	    update rs;
	    
	    Test.stopTest();
	    
	    GHI_DR_TemplateInfo info = GHI_DR_TemplateController.getTemplate(rs.Id, testTemplateVersion.Id);
	    
	    System.assertNotEquals(null, info.header);
	    System.assertEquals('some cool 10 text', info.header.Text_1__r.Text__c);
	    System.assertEquals(null, info.header.Text_2__c);
	    System.assertEquals('some cool 10 text', info.header.Text_3__r.Text__c);
	}
	
	private static testMethod void testTileTexts() {
	    Test.startTest();
	    
	    GHI_DR_Template_Header__c testHeader = GHI_DR_TestUtil.createHeader('test-header');
	    GHI_DR_Template_Footer__c testFooter = GHI_DR_TestUtil.createFooter('test-footer');
	    GHI_DR_Template__c testTemplate = GHI_DR_TestUtil.createTemplate('test-template');
	    
	    GHI_DR_Template_Header_Version__c testHeaderVersion = 
	        GHI_DR_TestUtil.createHeaderVersion('thv', 'thvComponent', testHeader.Id);
	        
	    GHI_DR_Template_Footer_Version__c testFooterVersion = 
	        GHI_DR_TestUtil.createFooterVersion('tfv', 'tfvComponent', testFooter.Id);
	        
	    GHI_DR_Template_Version__c testTemplateVersion =
	        GHI_DR_TestUtil.createTemplateVersion('ttv', testTemplate.Id, testHeaderVersion.Id, testFooterVersion.Id);

        GHI_DR_Drawer_Group__c drawerGroup = GHI_DR_TestUtil.createDrawerGroup('tdg');
        GHI_DR_Drawer_Group_Version__c groupVersion = GHI_DR_TestUtil.createDrawerGroupVersion('dgv', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem1 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', GHI_DR_TemplateInfo.USE_STATIC, groupVersion.Id);
        GHI_DR_Drawer_Item__c drawerItem2 = GHI_DR_TestUtil.createDrawerItem('label2', 'value', GHI_DR_TemplateInfo.USE_STATIC, groupVersion.Id);

        GHI_DR_Drawer_Group_Version__c groupVersion2 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv2', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem3 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', GHI_DR_TemplateInfo.USE_STATIC, groupVersion2.Id);
        
        GHI_DR_Drawer_Group_Version__c groupVersion3 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv3', drawerGroup.Id);        
        
        testHeaderVersion.Drawer_Group_1__c = groupVersion.Id;
        testHeaderVersion.Drawer_Group_2__c = groupVersion2.Id;
        testHeaderVersion.Drawer_Group_3__c = groupVersion3.Id;
        update testHeaderVersion;
        
        GHI_DR_Tile__c testTile1 = GHI_DR_TestUtil.createTile('test tile 1');
        
        GHI_DR_Tile__c testTile2 = GHI_DR_TestUtil.createTile('test tile 2');
        
        GHI_DR_Tile_Version__c tv1 = GHI_DR_TestUtil.createTileVersion('tv1', 'TileComponentName1', testTile1.Id, testTemplateVersion.Id);
        GHI_DR_Tile_Version__c tv2 = GHI_DR_TestUtil.createTileVersion('tv2', 'TileComponentName2', testTile2.Id, testTemplateVersion.Id);
	    
	    GHI_DR_Text__c text1 = GHI_DR_TestUtil.createText('testText1');
	    GHI_DR_Text_Version__c textV1 = GHI_DR_TestUtil.createTextVersion('testText1 - Eng - 1', 'some cool {{Recurrence__c}} text', text1.Id);
	    
        tv1.Text_1__c = textV1.Id;
	    tv1.Text_15__c = textV1.Id;
	    update tv1;
	    
	    testFooterVersion.Trademark_Notice__c = textV1.Id;
	    update testFooterVersion;
	    
	    GHI_DR_Result_Set__c rs = GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, true);
	    rs.Recurrence__c = 10;
	    update rs;
	    
	    Test.stopTest();	  
	    
	    GHI_DR_TemplateInfo info = GHI_DR_TemplateController.getTemplate(rs.Id, testTemplateVersion.Id);
	    
	    System.assertNotEquals(null, info.tiles);
	    System.assertEquals('some cool 10 text', info.tiles.get(0).Text_1__r.Text__c);
	    System.assertEquals(null, info.header.Text_2__c);
	    System.assertEquals('some cool 10 text', info.tiles.get(0).Text_15__r.Text__c);	 
	    System.assertEquals('some cool 10 text', info.footer.Trademark_Notice__r.Text__c);	 
	}
	
	private static testMethod void testDrawerItems() {
	    Test.startTest();
	    
	    GHI_DR_Template_Header__c testHeader = GHI_DR_TestUtil.createHeader('test-header');
	    GHI_DR_Template_Footer__c testFooter = GHI_DR_TestUtil.createFooter('test-footer');
	    GHI_DR_Template__c testTemplate = GHI_DR_TestUtil.createTemplate('test-template');
	    
	    GHI_DR_Template_Header_Version__c testHeaderVersion = 
	        GHI_DR_TestUtil.createHeaderVersion('thv', 'thvComponent', testHeader.Id);
	        
	    GHI_DR_Template_Footer_Version__c testFooterVersion = 
	        GHI_DR_TestUtil.createFooterVersion('tfv', 'tfvComponent', testFooter.Id);
	        
	    GHI_DR_Template_Version__c testTemplateVersion =
	        GHI_DR_TestUtil.createTemplateVersion('ttv', testTemplate.Id, testHeaderVersion.Id, testFooterVersion.Id);

        GHI_DR_Drawer_Group__c drawerGroup = GHI_DR_TestUtil.createDrawerGroup('tdg');
        GHI_DR_Drawer_Group_Version__c groupVersion = GHI_DR_TestUtil.createDrawerGroupVersion('dgv', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem1 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', '', groupVersion.Id);
        GHI_DR_Drawer_Item__c drawerItem2 = GHI_DR_TestUtil.createDrawerItem('label2', 'value', '', groupVersion.Id);

        GHI_DR_Drawer_Group_Version__c groupVersion2 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv2', drawerGroup.Id);
        GHI_DR_Drawer_Item__c drawerItem3 = GHI_DR_TestUtil.createDrawerItem('label1', '"value"', 'Report_Date__c', groupVersion2.Id);
        drawerItem3.Data_Type__c = 'Date';
        update drawerItem3;
        
        GHI_DR_Drawer_Group_Version__c groupVersion3 = GHI_DR_TestUtil.createDrawerGroupVersion('dgv3', drawerGroup.Id);        
        
        testHeaderVersion.Drawer_Group_1__c = groupVersion.Id;
        testHeaderVersion.Drawer_Group_2__c = groupVersion2.Id;
        testHeaderVersion.Drawer_Group_3__c = groupVersion3.Id;
        update testHeaderVersion;
        
        GHI_DR_Tile__c testTile1 = GHI_DR_TestUtil.createTile('test tile 1');
        
        GHI_DR_Tile__c testTile2 = GHI_DR_TestUtil.createTile('test tile 2');
        
        GHI_DR_Tile_Version__c tv1 = GHI_DR_TestUtil.createTileVersion('tv1', 'TileComponentName1', testTile1.Id, testTemplateVersion.Id);
        GHI_DR_Tile_Version__c tv2 = GHI_DR_TestUtil.createTileVersion('tv2', 'TileComponentName2', testTile2.Id, testTemplateVersion.Id);

	    GHI_DR_Result_Set__c rs = GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, true);
	    rs.Report_Date__c = System.today();
	    update rs;
	    
	    Test.stopTest();
	    
	    GHI_DR_TemplateInfo info = GHI_DR_TemplateController.getTemplate(rs.Id, testTemplateVersion.Id);
	    
	    System.assertNotEquals(null, info.tiles);	    
	}
	
	private static testMethod void testSetPDFPReference() {
	    GHI_DR_TemplateController.setPDFPreference('testing');
	    
	    User currentUser  =
	        [SELECT GHI_DR_PDF_Preference__c 
	           FROM User 
	          WHERE Id = :UserInfo.getUserId()];
	          
	    System.assertEquals('testing', currentUser.GHI_DR_PDF_Preference__c);
	}
	
	private static testMethod void testEncryption() {
	    String toEncrypt = 'test';
	    
	    System.assertNotEquals(toEncrypt, GHI_DR_TemplateController.encrypt(toEncrypt));
	}

}