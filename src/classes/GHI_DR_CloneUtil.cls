public with sharing class GHI_DR_CloneUtil {
    
    public static final Integer DEFAULT_VERSION = 1;
    
    public static final List<String> TEXTS_TEMPLATE_VERSION = new List<String>{
        'Color_Version__c', 
        'Fax_Version__c',
        'PDF_Button_Label__c',
        'PDF_Options__c',
        'Print_Option_Bottom__c',
        'Print_Option_Middle__c',
        'Print_Option_Top__c',
        'Report_Overview__c',
        'Standard_Report_Button_Label__c',
        'Subtitle__c',
        'Title__c',
        'Patient_Summary__c'
    };
    
    public static final List<String> TEXTS_TILE_VERSION = new List<String>{
        'Full_Subtitle__c',
        'Full_Title__c',
        'Full_Title_Right__c',
        'Subtitle__c',
        'Title__c',
        'Text_1__c',
        'Text_2__c',
        'Text_3__c',
        'Text_4__c',
        'Text_5__c',
        'Text_6__c',
        'Text_7__c',
        'Text_8__c',
        'Text_9__c',
        'Text_10__c',
        'Text_11__c',
        'Text_12__c',
        'Text_13__c',
        'Text_14__c',
        'Text_15__c',
        'Text_16__c',
        'Text_17__c',
        'Text_18__c',
        'Text_19__c',
        'Text_20__c',
        'Text_21__c',
        'Text_22__c',
        'Text_23__c',
        'Text_24__c',
        'Text_25__c',
        'Text_26__c',
        'Text_27__c',
        'Text_28__c',
        'Text_29__c',
        'Text_30__c'        
    };
    
    public static final List<String> TEXTS_FOOTER_VERSION = new List<String>{
        'Address_1__c',
        'Address_2__c',
        'Address_3__c',
        'CLIA__c',
        'In_Range_Clinical_Validation__c',
        'Lab_Director_Body__c',
        'Link_1_Text__c',
        'Link_2_Text__c',
        'Link_3_Text__c',
        'OOR_Clinical_Validation__c',
        'Phone_Number__c',
        'Trademark_Notice__c',
        'Website__c',
        'Very_Low_Clinical_Validation__c',
        'Low_Clinical_Validation__c',
        'Int_Clinical_Validation__c'
    };
    
    public static final List<String> TEXTS_HEADER_VERSION = new List<String>{
        'Text_1__c',
        'Text_2__c',
        'Text_3__c',
        'Text_4__c',
        'Text_5__c',
        'Text_6__c'
    };

    public static Id createNewTemplateVersion(Id tvId) {
        List<GHI_DR_Template_Version__c> templateVersions = getTemplateVersions(tvId);
    
        if(!templateVersions.isEmpty()) {
            GHI_DR_Template_Version__c base = templateVersions.get(0);
            base = base.clone(false, true);
            Map<String, Id> fieldToId = new Map<String, Id>();
            
            for(String field : TEXTS_TEMPLATE_VERSION) {
                fieldToId.put(field, (Id) base.get(field));
            }
            
            fieldToId = cloneTextVersions(tvId, fieldToId);
            
            for(String field : fieldToId.keySet()) {
                base.put(field, fieldToId.get(field));
            }            
            
            GHI_DR_Template_Footer_Version__c fv = 
                cloneFooterVersion(base.GHI_DR_Template_Footer_Version__c);
                
            GHI_DR_Template_Header_Version__c hv = 
                cloneHeaderVersion(base.GHI_DR_Template_Header_Version__c);
            
            base.GHI_DR_Template_Footer_Version__c = fv.Id;
            base.GHI_DR_Template_Header_Version__c = hv.Id;
            
            base.Version_Number__c = 
                base.Version_Number__c == null ?
                DEFAULT_VERSION + 1 : base.Version_Number__c + 1;
            
            insert base;
            
            cloneTileVersions(templateVersions.get(0).Id, base.Id);
            
            return base.Id;
        }
        
        return null;
    }
    
    private static void cloneTileVersions(Id oldTemplateVersionId, Id newTemplateVersionId) {
        List<GHI_DR_Tile_Version__c> tiles = getTileVersions(oldTemplateVersionId);
    
        if(!tiles.isEmpty()) {
            List<GHI_DR_Tile_Version__c> clonedTiles = new List<GHI_DR_Tile_Version__c>();
            Map<Id, Map<String, Id>> tileIdToTexts = new Map<Id, Map<String, Id>>();
            Map<String, Id> tempTexts;
            GHI_DR_Tile_Version__c base;
            
            for(GHI_DR_Tile_Version__c tile : tiles) {
                tileIdToTexts.put(tile.Id, new Map<String, Id>());
        
                tempTexts = tileIdToTexts.get(tile.Id);
                for(String key : TEXTS_TILE_VERSION) {
                    tempTexts.put(key, (Id) tile.get(key));
                }
            }
            
            tileIdToTexts = cloneTextVersions(tileIdToTexts);
            
            for(GHI_DR_Tile_Version__c tile : tiles) {
                base = tile.clone(false, true);
                tempTexts = tileIdToTexts.get(tile.Id);
                
                for(String key : tempTexts.keySet()) {
                    base.put(key, tempTexts.get(key));
                }
                
                base.GHI_DR_Template_Version__c = newTemplateVersionId;
                base.Version_Number__c = 
                    base.Version_Number__c == null ?
                    DEFAULT_VERSION + 1 : base.Version_Number__c + 1;
                    
                clonedTiles.add(base);
            }
            
            insert clonedTiles;
        }
    }
    
    private static GHI_DR_Template_Footer_Version__c cloneFooterVersion(Id tfvId) {
        List<GHI_DR_Template_Footer_Version__c> footers = getFooterVersions(tfvId);
        
        if(!footers.isEmpty()) {
            GHI_DR_Template_Footer_Version__c base = footers.get(0);
            base = base.clone(false, true);
            
            Map<String, Id> fieldToId = new Map<String, Id>();
            
            for(String field : TEXTS_FOOTER_VERSION) {
                fieldToId.put(field, (Id) base.get(field));
            }
            
            fieldToId = cloneTextVersions(tfvId, fieldToId);
            
            for(String field : fieldToId.keySet()) {
                base.put(field, fieldToId.get(field));
            }            
            
            base.Version_Number__c = 
                base.Version_Number__c == null ?
                DEFAULT_VERSION + 1 : base.Version_Number__c + 1;            
            
            insert base;
            return base;
        }
        
        return null;
    }
    
    private static GHI_DR_Template_Header_Version__c cloneHeaderVersion(Id thvId) {
        List<GHI_DR_Template_Header_Version__c> headers = getHeaderVersions(thvId);
        
        if(!headers.isEmpty()) {
            GHI_DR_Template_Header_Version__c base = headers.get(0);
            base = base.clone(false, true);
            
            Map<String, Id> fieldToId = new Map<String, Id>();
            
            for(String field : TEXTS_HEADER_VERSION) {
                fieldToId.put(field, (Id) base.get(field));
            }
            
            fieldToId = cloneTextVersions(thvId, fieldToId);
            
            for(String field : fieldToId.keySet()) {
                base.put(field, fieldToId.get(field));
            }
            
            base.Drawer_Group_1__c = 
                cloneDrawerGroupVersion(base.Drawer_Group_1__c);
                
            base.Drawer_Group_2__c = 
                cloneDrawerGroupVersion(base.Drawer_Group_2__c);
                
            base.Drawer_Group_3__c = 
                cloneDrawerGroupVersion(base.Drawer_Group_3__c);

            base.Version_Number__c = 
                base.Version_Number__c == null ?
                DEFAULT_VERSION + 1 : base.Version_Number__c + 1;
            
            insert base;
            return base;
        }
        
        return null;
    }
    
    private static Id cloneDrawerGroupVersion(Id dgvId) {
        if(dgvId != null) {
            List<GHI_DR_Drawer_Group_Version__c> dgvs = getDrawerGroupVersion(dgvId);
                  
            if(!dgvs.isEmpty()) {
                GHI_DR_Drawer_Group_Version__c base = dgvs.get(0).clone(false, true);
                insert base;
                
                List<GHI_DR_Drawer_Item__c> items = getDrawerItem(dgvId);
                
                List<GHI_DR_Drawer_Item__c> clonedItems = new List<GHI_DR_Drawer_Item__c>();
                GHI_DR_Drawer_Item__c temp;
                
                for(GHI_DR_Drawer_Item__c item : items) {
                    temp = item.clone(false, true);
                    temp.GHI_DR_Drawer_Group_Version__c = base.Id;
                    
                    clonedItems.add(temp);
                }

                base.Version_Number__c = 
                    base.Version_Number__c == null ?
                    DEFAULT_VERSION + 1 : base.Version_Number__c + 1;
                
                insert clonedItems;
                return base.Id;
            }
        }
        
        return null;
    }
    
    private static Map<String, Id> cloneTextVersions(Id sourceObjectId, Map<String, Id> fieldToId) {
        Map<Id, Map<String, Id>> recordIdToTexts = new Map<Id, Map<String, Id>>{
            sourceObjectId => fieldToId
        };
        
        recordIdToTexts = cloneTextVersions(recordIdToTexts);
        
        return recordIdToTexts.get(sourceObjectId);
    }
    
    private static Map<Id, Map<String, Id>> cloneTextVersions(Map<Id, Map<String, Id>> tileIdToTexts) {
        Set<Id> textVersionIds = new Set<Id>();
        
        Map<String, Id> tempTexts;
        for(Id key : tileIdToTexts.keySet()) {
            tempTexts = tileIdToTexts.get(key);
            
            for(String field : tempTexts.keySet()) {
                textVersionIds.add(tempTexts.get(field));
            }
        }
        
        List<GHI_DR_Text_Version__c> texts =
            [SELECT Id,
                    Name,
                    GHI_DR_Text__c,
                    Language__c,
                    Text__c
               FROM GHI_DR_Text_Version__c
              WHERE Id in :textVersionIds];   

        Map<Id, GHI_DR_Text_Version__c> idToClone = new Map<Id, GHI_DR_Text_Version__c>();
        GHI_DR_Text_Version__c clonedText;
        for(GHI_DR_Text_Version__c tv : texts) {
            clonedText = tv.clone(false, true);

            clonedText.Version_Number__c = 
                clonedText.Version_Number__c == null ?
                DEFAULT_VERSION + 1 : clonedText.Version_Number__c + 1;            
            
            idToClone.put(tv.Id, clonedText);
        }
        
        insert idToClone.values();

        Map<Id, Map<String, Id>> clonedMap = new Map<Id, Map<String, Id>>();
        Id oldId;
        Id newId;
        
        for(Id key : tileIdToTexts.keySet()) {
            tempTexts = tileIdToTexts.get(key);
            clonedMap.put(key, new Map<String, Id>());
            
            for(String field : tempTexts.keySet()) {
                oldId = tempTexts.get(field);
                
                newId = oldId != null && idToClone.containsKey(oldId) ?
                        idToClone.get(oldId).Id : null;
                        
                clonedMap.get(key).put(field, newId);
            }
        }
              
        return clonedMap;
    }
    
    private static List<GHI_DR_Drawer_Item__c> getDrawerItem(Id dgvId) {
        return
            [SELECT Id,
                    Name,
                    Data_Type__c,
                    Exclude_After_First__c,
                    Field_Value__c,
                    Font_Size__c,
                    Label__c,
                    Label_Bold__c,
                    Order__c,
                    Static_Value__c,
                    Use_Colon_Separator__c,
                    Value_Bold__c,
                    Display_N_A_for_Blank__c,
                    Hide_If_Blank__c,
                    GHI_DR_Drawer_Group_Version__c
               FROM GHI_DR_Drawer_Item__c
              WHERE GHI_DR_Drawer_Group_Version__c = :dgvId];       
    }
    
    private static List<GHI_DR_Drawer_Group_Version__c> getDrawerGroupVersion(Id dgvId) {
        return
            [SELECT Id,
                    Name,
                    Language__c,
                    Version_Number__c,
                    GHI_DR_Drawer_Group__c
               FROM GHI_DR_Drawer_Group_Version__c
              WHERE Id = :dgvId];        
    }
    
    private static List<GHI_DR_Tile_Version__c> getTileVersions(Id tileVersionId) {
        return
            [SELECT Id,
                    Name,
                    Version_Number__c,
                    Exclude_Page_Number__c,
                    GHI_DR_Tile__c,
                    Has_Extended_Footer__c,
                    Has_Extended_Header__c,
                    Language__c,
                    Order__c,
                    Page_Number__c,
                    Position__c,
                    GHI_DR_Template_Version__c,
                    Full_Subtitle__c,
                    Full_Title__c,
                    Full_Title_Right__c,
                    Subtitle__c,
                    Title__c,
                    Text_1__c,
                    Text_2__c,
                    Text_3__c,
                    Text_4__c,
                    Text_5__c,
                    Text_6__c,
                    Text_7__c,
                    Text_8__c,
                    Text_9__c,
                    Text_10__c,
                    Text_11__c,
                    Text_12__c,
                    Text_13__c,
                    Text_14__c,
                    Text_15__c,
                    Text_16__c,
                    Text_17__c,
                    Text_18__c,
                    Text_19__c,
                    Text_20__c,
                    Text_21__c,
                    Text_22__c,
                    Text_23__c,
                    Text_24__c,
                    Text_25__c,
                    Text_26__c,
                    Text_27__c,
                    Text_28__c,
                    Text_29__c,
                    Text_30__c
               FROM GHI_DR_Tile_Version__c
              WHERE GHI_DR_Template_Version__c = :tileVersionId];
    }
    
    private static List<GHI_DR_Template_Footer_Version__c> getFooterVersions(Id footerVersionId) {
        return
            [SELECT Id,
                    Name,
                    Version_Number__c,
                    GHI_DR_Template_Footer__c,
                    Link_1_URL__c,
                    Link_2_URL__c,
                    Language__c,
                    Logo_Link__c,
                    Logo_URL__c,
                    Address_1__c,
                    Address_2__c,
                    Address_3__c,
                    CLIA__c,
                    In_Range_Clinical_Validation__c,
                    Lab_Director__c,
                    Lab_Director_Body__c,
                    Link_1_Text__c,
                    Link_2_Text__c,
                    Link_3_Text__c,
                    OOR_Clinical_Validation__c,
                    Phone_Number__c,
                    Trademark_Notice__c,
                    Website__c
               FROM GHI_DR_Template_Footer_Version__c
              WHERE Id = :footerVersionId];        
    }
    
    private static List<GHI_DR_Template_Header_Version__c> getHeaderVersions(Id headerVersionId) {
        return
            [SELECT Id,
                    Name,
                    Version_Number__c,
                    GHI_DR_Template_Header__c,
                    Header_Color__c,
                    Header_Image_Link__c,
                    Header_Image_URL__c,
                    Language__c,
                    Text_1__c,
                    Text_2__c,
                    Text_3__c,
                    Text_4__c,
                    Text_5__c,
                    Drawer_Group_1__c,
                    Drawer_Group_2__c,
                    Drawer_Group_3__c,
                    Group_1_Pages_Excluded__c,
                    Group_2_Pages_Excluded__c,
                    Group_3_Pages_Excluded__c
               FROM GHI_DR_Template_Header_Version__c
              WHERE Id = :headerVersionId];        
    }
    
    private static List<GHI_DR_Template_Version__c> getTemplateVersions(Id tvId) {
        return
            [SELECT Id,
                    Name,
                    Version_Number__c,
                    Active_Start_Date__c,
                    Base_Component__c,
                    Language__c,
                    Print_Option_Type_Bottom__c,
                    Print_Option_Type_Middle__c,
                    Print_Option_Type_Top__c,
                    GHI_DR_Template__c,
                    Preview_With__c,
                    GHI_DR_Template_Footer_Version__c, 
                    GHI_DR_Template_Header_Version__c, 
                    Color_Version__c, 
                    Fax_Version__c,        
                    Patient_Summary__c,             
                    PDF_Button_Label__c, 
                    PDF_Options__c,                     
                    Print_Option_Bottom__c, 
                    Print_Option_Middle__c, 
                    Print_Option_Top__c,                     
                    Report_Overview__c, 
                    Standard_Report_Button_Label__c, 
                    Subtitle__c, 
                    Title__c 
               FROM GHI_DR_Template_Version__c
              WHERE Id = :tvId];        
    }    

}