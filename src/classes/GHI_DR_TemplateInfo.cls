public with sharing class GHI_DR_TemplateInfo {

    private static final Integer ROW_MAX        = 100;
    private static final Integer TEST_MAX       = 100;
    private static final Integer DECIMAL_LENGTH = 1;
    private static final String  QUOTATION_MARK = '"';
    private static final String  DATA_TYPE_DATE = 'Date';
    private static final String  OPEN_MERGE     = '\\{\\{';
    private static final String  CLOSE_MERGE    = '\\}\\}';
    private static final String  DEF_PDF_PREF   = 'Fax (Black & White)';
    private static final Integer NUM_TILE_TEXTS = 30;
    private static final Integer NUM_HEAD_TEXTS = 4;
    public  static final String  USE_STATIC     = '-- Use Static Value --';

    public GHI_DR_Result_Set__c              report       {get; set;}    
    public GHI_DR_Template_Version__c        template     {get; set;}
    public GHI_DR_Template_Footer_Version__c footer       {get; set;}
    public GHI_DR_Template_Header_Version__c header       {get; set;}
    public List<GHI_DR_Tile_Version__c>      tiles        {get; set;}
    public List<GHI_DR_Drawer_Item__c>       drawerGroup1 {get; set;}
    public List<GHI_DR_Drawer_Item__c>       drawerGroup2 {get; set;}
    public List<GHI_DR_Drawer_Item__c>       drawerGroup3 {get; set;}
    public String                            pdfPref      {get; set;}
    public Id                                oldReportId  {get; set;}
    
    public GHI_DR_TemplateInfo(Id resultId, Id templateId) {
        Pattern pat = Pattern.compile('(' + OPEN_MERGE + '[\\d\\w_]+' + CLOSE_MERGE + ')');
        report      = getReportData(resultId);
        
        template    = getTemplate(templateId);
        header      = getHeader(template, pat, report);
        footer      = getFooter(template, pat, report);
        tiles       = getTiles(templateId, pat, report);
        oldReportId = getOldReportId();
        pdfPref     = getPDFPreference();
        
        setDrawerGroups(report);
    }
    
    private String getPDFPreference() {
        List<User> users = 
            [SELECT GHI_DR_PDF_Preference__c 
               FROM User 
              WHERE Id = :UserInfo.getUserId()];
              
        if(!users.isEmpty()) {
            String userPreference = users.get(0).GHI_DR_PDF_Preference__c;
            
            if(String.isNotBlank(userPreference)) {
                return userPreference;
            }
        }
        
        return DEF_PDF_PREF;
    }
    
    private Id getOldReportId() {
        if(report != null) {
            String oldId = report.Distribution_Event_Report_Id__c;
            
            if(String.isNotBlank(oldId)) {
                List<OSM_Distribution_Event__c> events =
                    [SELECT Id
                       FROM OSM_Distribution_Event__c
                      WHERE OSM_Report_Id__c = :oldId
                      LIMIT 1];
                      
                if(!events.isEmpty()) {
                    return events.get(0).Id;
                }
            }
            
        }
        
        return null;
    }
    
    private GHI_DR_Result_Set__c getReportData(Id resultId) {
        List<GHI_DR_Result_Set__c> reports = 
            [SELECT Id,
                    Name,
                    Absolute_Chemotherapy_Benefit__c,
                    Additional_Recipient__c,
                    Age_at_Biopsy__c,
                    B14_Defined__c,
                    B14_LC_High_Range__c,
                    B14_LC_Low_Range__c,
                    B14_LC_Units__c,
                    B14_LC_Unrounded__c,
                    B14_LC_Value__c,
                    B14_Rate_High_Range__c,
                    B14_Rate_Low_Range__c,
                    B14_Rate_Units__c,
                    B14_Rate_Unrounded__c,
                    B14_Rate_Value__c,
                    B14_UC_High_Range__c,
                    B14_UC_Low_Range__c,
                    B14_UC_Units__c,
                    B14_UC_Unrounded__c,
                    B14_UC_Value__c,
                    B14_Value__c,
                    B20_Value__c,
                    Client__c,
                    Clinical_Stage__c,
                    Date_of_Biopsy__c,
                    Date_of_Collection__c,
                    Date_of_Diagnosis__c,
                    Distribution_Event__c,
                    ER_Classification__c,
                    ER_Gene_Value__c,
                    ER_Gene_Value_High_Range__c,
                    ER_Gene_Value_Low_Range__c,
                    ER_Gene_Value_Unrounded__c,
                    ER_Range__c,
                    GPS_High_Range__c,
                    GPS_In_Range__c,
                    GPS_Low_Range__c,
                    GPS_Unrounded__c,
                    GPS_Value__c,
                    Gleason_Score__c,
                    HER2_Classification__c,
                    HER2_Gene_Value__c,
                    HER2_Gene_Value_High_Range__c,
                    HER2_Gene_Value_Low_Range__c,
                    HER2_Gene_Value_Unrounded__c,
                    HER2_Range__c,
                    High_Range__c,
                    Lab_Directors__c,
                    Low_Range__c,
                    Max_tumor_involvement__c,
                    Medical_Record_Patient__c,
                    NCCN_Risk_Group__c,
                    NCCN_Category__c,
                    Number_cores_positive_collected__c,
                    Order_Number__c,
                    Ordering_Physician__c,
                    PR_Classification__c,
                    PR_Gene_Value__c,
                    PR_Gene_Value_High_Range__c,
                    PR_Gene_Value_Low_Range__c,
                    PR_Gene_Value_Unrounded__c,
                    PR_Range__c,
                    PSA__c,
                    PSA_Density__c,
                    Patient_Summary_Value__c,
                    Patient_Birthdate__c,
                    Patient_Gender__c,
                    Patient_Name__c,
                    Prostate_Volume__c,
                    Recurrence__c,
                    Recurrence_Unrounded__c,
                    Report_Number__c,
                    Report_Date__c,
                    Specimen_Received__c,
                    Specimen_Type_ID__c,
                    Study__c,
                    Distribution_Event_Report_Id__c,
                    Print_URL__c,
                    Fax_URL__c
               FROM GHI_DR_Result_Set__c 
              WHERE Id = :resultId];
            
        if(!reports.isEmpty()) {
            return reports.get(0);
        }
        
        return null;
    }
    
    private void setDrawerGroups(GHI_DR_Result_Set__c report) {
        if(header != null) {
            if(String.isNotBlank(header.Drawer_Group_1__c)) {
                drawerGroup1 = getDrawerGroupItems(report, header.Drawer_Group_1__c);
            }
            
            if(String.isNotBlank(header.Drawer_Group_2__c)) {
                drawerGroup2 = getDrawerGroupItems(report, header.Drawer_Group_2__c);
            }
            
            if(String.isNotBlank(header.Drawer_Group_3__c)) {
                drawerGroup3 = getDrawerGroupItems(report, header.Drawer_Group_3__c);
            }
        }
    }
    
    private List<GHI_DR_Drawer_Item__c> updateDrawerItemValues(GHI_DR_Result_Set__c report, List<GHI_DR_Drawer_Item__c> items) {
        if(report == null) {
            return items;
        }
        
        Date tempDate;
        Datetime tempDateTime;
        
        for(GHI_DR_Drawer_Item__c item : items) {
            if(String.isNotBlank(item.Field_Value__c) && item.Field_Value__c != USE_STATIC) {
                if(report.get(item.Field_Value__c) == null) {
                    item.Static_Value__c = '';
                } else {
                    if(item.Data_Type__c == DATA_TYPE_DATE) {
                        tempDate = (Date) report.get(item.Field_Value__c);
                        tempDateTime = Datetime.newInstance(
                            tempDate.year(), 
                            tempDate.month(), 
                            tempDate.day());
                            
                        item.Static_Value__c = String.valueOf(tempDateTime.getTime());
                    } else {
                        if(item.Dynamic_Value__c != null) {
                            item.Static_Value__c = String.valueOf(item.Dynamic_Value__r.Text__c);    
                        }else{
                            item.Static_Value__c = String.valueOf(report.get(item.Field_Value__c));    
                        }
                    }
                }
            }
        }
        
        return items;
    }
    
    private List<GHI_DR_Drawer_Item__c> getDrawerGroupItems(GHI_DR_Result_Set__c report, Id drawerId) {
        List<GHI_DR_Drawer_Item__c> items =
            [SELECT Label__c, 
                    Exclude_After_First__c,
                    Font_Size__c, 
                    Order__c, 
                    Label_Bold__c, 
                    Value_Bold__c,
                    Use_Colon_Separator__c,
                    Field_Value__c,
                    Static_Value__c,
                    Dynamic_Value__c,
                    Dynamic_Value__r.Text__c,
                    Data_Type__c,
                    Display_N_A_for_Blank__c,
                    Hide_If_Blank__c
               FROM GHI_DR_Drawer_Item__c 
              WHERE GHI_DR_Drawer_Group_Version__c = :drawerId
           ORDER BY Order__c ASC];
           
        return updateDrawerItemValues(report, items);
    }
    
    private GHI_DR_Template_Version__c getTemplate(Id templateId) {
        if(String.isNotBlank(templateId)) {
            return [SELECT Id, 
                           Name,
                           Active_Start_Date__c,
                           GHI_DR_Template_Footer_Version__c,
                           GHI_DR_Template_Header_Version__c,
                           Title__r.Text__c,
                           Subtitle__r.Text__c,
                           Standard_Report_Button_Label__r.Text__c,
                           PDF_Button_Label__r.Text__c,
                           Report_Overview__r.Text__c,
                           PDF_Options__r.Text__c,
                           Fax_Version__r.Text__c,
                           Color_Version__r.Text__c,
                           Print_Option_Bottom__r.Text__c,
                           Print_Option_Middle__r.Text__c,
                           Print_Option_Top__r.Text__c,
                           Print_Option_Type_Bottom__c,
                           Print_Option_Type_Middle__c,
                           Print_Option_Type_Top__c,
                           Patient_Summary__r.Text__c,
                           Digital_Document_Id__r.Text__c,
                           Fax_Template__r.Base_Component__c,
                           Print_Template__r.Base_Component__c
                      FROM GHI_DR_Template_Version__c
                     WHERE Id = :templateId];
        }
        
        return null;
    }
    
    private GHI_DR_Template_Footer_Version__c getFooter(GHI_DR_Template_Version__c main, 
                                                        Pattern pat, 
                                                        GHI_DR_Result_Set__c resultSet) {
        if(main != null) {
            return updateFooterTexts([SELECT Id,
                           Name,
                           GHI_DR_Template_Footer__r.Component_Name__c,
                           Logo_URL__c,
                           Logo_Link__c,
                           Link_1_URL__c,
                           Address_1__r.Text__c,
                           Address_2__r.Text__c,
                           Address_3__r.Text__c,
                           Link_1_Text__r.Text__c,
                           Link_2_Text__r.Text__c,
                           Link_3_Text__r.Text__c,
                           CLIA__r.Text__c,
                           Lab_Director__r.Text__c,
                           In_Range_Clinical_Validation__r.Text__c,
                           OOR_Clinical_Validation__r.Text__c,
                           Trademark_Notice__r.Text__c,
                           Website__r.Text__c,
                           Phone_Number__r.Text__c,
                           Lab_Director_Body__r.Text__c,
                           Int_Clinical_Validation__r.Text__c,
                           Very_Low_Clinical_Validation__r.Text__c,
                           Low_Clinical_Validation__r.Text__c
                      FROM GHI_DR_Template_Footer_Version__c
                     WHERE Id = :main.GHI_DR_Template_Footer_Version__c], pat, resultSet);
        }
        
        return null;
    }
    
    private GHI_DR_Template_Header_Version__c getHeader(GHI_DR_Template_Version__c main, Pattern pat, GHI_DR_Result_Set__c resultSet) {
        if(main != null) {
            return updateHeaderTexts([SELECT Id,
                           Name,
                           GHI_DR_Template_Header__r.Component_Name__c,
                           Header_Color__c,
                           Header_Image_URL__c,
                           Header_Image_Link__c,
                           Drawer_Group_1__c,
                           Drawer_Group_2__c,
                           Drawer_Group_3__c,
                           Text_1__r.Text__c,
                           Text_2__r.Text__c,
                           Text_3__r.Text__c,
                           Text_4__r.Text__c,
                           Text_5__r.Text__c,
                           Text_6__r.Text__c,
                           Group_1_Pages_Excluded__c,
                           Group_2_Pages_Excluded__c,
                           Group_3_Pages_Excluded__c
                      FROM GHI_DR_Template_Header_Version__c
                     WHERE Id = :main.GHI_DR_Template_Header_Version__c], pat, resultSet);            
        }
        
        return null;
    }
    
    private List<GHI_DR_Tile_Version__c> getTiles(Id templateId, Pattern pat, GHI_DR_Result_Set__c resultSet) {
        if(String.isNotBlank(templateId)) {
            return updateTileTexts(
                mergeTileTexts([SELECT Id,
                        Name,
                        GHI_DR_Tile__r.Component_Name__c,
                        GHI_DR_Tile__r.Is_Summary_Page__c,
                        Has_Extended_Footer__c,
                        Exclude_Page_Number__c,
                        Text_1__c,
                        Text_2__c,
                        Text_3__c,
                        Text_4__c,
                        Text_5__c,
                        Text_6__c,
                        Text_7__c,
                        Text_8__c,
                        Text_9__c,
                        Text_10__c,
                        Text_11__c,
                        Text_12__c,
                        Text_13__c,
                        Text_14__c,
                        Text_15__c,
                        Text_16__c,
                        Text_17__c,
                        Text_18__c,
                        Text_19__c,
                        Text_20__c,
                        Text_21__c,
                        Text_22__c,
                        Text_23__c,
                        Text_24__c,
                        Text_25__c,
                        Text_26__c,
                        Text_27__c,
                        Text_28__c,
                        Text_29__c,
                        Text_30__c,
                        Title__r.Text__c,
                        Subtitle__r.Text__c,
                        Full_Title__r.Text__c,
                        Full_Title_Right__r.Text__c,
                        Full_Subtitle__r.Text__c,
                        Page_Number__c,
                        Position__c,
                        Has_Extended_Header__c,
                        PDF_Document_Id__r.Text__c
                   FROM GHI_DR_Tile_Version__c
                  WHERE GHI_DR_Template_Version__c = :templateId
               ORDER BY Order__c ASC
                  LIMIT :ROW_MAX]), pat, resultSet);
        }
        
        return null;
    }

    private GHI_DR_Template_Footer_Version__c updateFooterTexts(List<GHI_DR_Template_Footer_Version__c> footerVersions, Pattern pat, GHI_DR_Result_Set__c resultSet) {
        String cField;
        String rField;
        Id currentId;
        GHI_DR_Text_Version__c currentText;
        GHI_DR_Template_Footer_Version__c current = footerVersions.get(0);
        List<String> updatable = new List<String>{
            'In_Range_Clinical_Validation__c', 
            'OOR_Clinical_Validation__c',
            'Trademark_Notice__c'
        };
        
        for(Integer i = 0; i < updatable.size(); i++) {
            cField = updatable.get(i);
            rField = updatable.get(i).replace('__c', '__r');
    
            currentId = (Id) current.get(cField);
            
            if(currentId != null) {
                currentText = (GHI_DR_Text_Version__c) current.getSObject(rField);
                
                if(String.isNotBlank(currentText.Text__c)) {
                    currentText.Text__c = injectFieldValues(pat, currentText.Text__c, resultSet);
                    current.putSobject(rField, currentText);
                }
            }
        }

        return current;
    }

    private GHI_DR_Template_Header_Version__c updateHeaderTexts(List<GHI_DR_Template_Header_Version__c> headerVersions, Pattern pat, GHI_DR_Result_Set__c resultSet) {
        String relationshipField;
        String cField;
        String rField;
        Id currentId;
        GHI_DR_Text_Version__c currentText;
        GHI_DR_Template_Header_Version__c current = headerVersions.get(0);
        
        for(Integer i = 1; i <= NUM_HEAD_TEXTS; i++) {
            relationshipField = 'Text_' + i + '__';
            cField = relationshipField + 'c';
            rField = relationshipField + 'r';
    
            currentId = (Id) current.get(cField);
            
            if(currentId != null) {
                currentText = (GHI_DR_Text_Version__c) current.getSObject(rField);
                
                if(String.isNotBlank(currentText.Text__c)) {
                    currentText.Text__c = injectFieldValues(pat, currentText.Text__c, resultSet);
                    current.putSobject(rField, currentText);
                }
            }
        }
        
        return current;
    }
    
    private List<GHI_DR_Tile_Version__c> updateTileTexts(List<GHI_DR_Tile_Version__c> tileVersions, Pattern pat, GHI_DR_Result_Set__c resultSet) {
        String relationshipField;
        String cField;
        String rField;
        Id currentId;
        GHI_DR_Text_Version__c currentText;
        
        for(GHI_DR_Tile_Version__c current : tileVersions) {
            for(Integer i = 1; i <= NUM_TILE_TEXTS; i++) {
                relationshipField = 'Text_' + i + '__';
                cField = relationshipField + 'c';
                rField = relationshipField + 'r';
        
                currentId = (Id) current.get(cField);
                
                if(currentId != null) {
                    currentText = (GHI_DR_Text_Version__c) current.getSObject(rField);
                    
                    if(String.isNotBlank(currentText.Text__c)) {
                        currentText.Text__c = injectFieldValues(pat, currentText.Text__c, resultSet);
                        current.putSobject(rField, currentText);
                    }
                }
            }
        }
        
        return tileVersions;
    }
    
    private String injectFieldValues(Pattern compiled, String searchText, GHI_DR_Result_Set__c resultSet) {
        if(String.isBlank(searchText) || resultSet == null) {
            return '';
        }
        
        Matcher mat = compiled.matcher(searchText);
        
        String currentItem;
        String currentField;
        String currentValue;
        
        while(mat.find()) {
            currentItem  = mat.group(1);
            currentField = currentItem.replace('{{', '').replace('}}', '');
            
            if(currentField.contains('__c') && resultSet.get(currentField) != null) {
                currentValue = String.valueOf(resultSet.get(currentField));
                searchText   = searchText.replace(currentItem, currentValue);                
            }
        }
        
        return searchText;
    }
    
    //Workaround for 35 relationship SOQL query governor limit
    private List<GHI_DR_Tile_Version__c> mergeTileTexts(List<GHI_DR_Tile_Version__c> tiles) {
        Set<Id> tileTexts   = new Set<Id>();
        String fieldBase    = 'Text_';
        String fieldEnd     = '__c';
        String relationship = '__r';
        Object current;
        Id currentId;
        
        for(GHI_DR_Tile_Version__c tile : tiles) {
            for(Integer i = 1; i <= NUM_TILE_TEXTS; i++) {
                current = tile.get(fieldBase + i + fieldEnd);
                
                if(current != null) {
                    tileTexts.add((Id) current);
                }
            }
        }
        
        Map<Id, GHI_DR_Text_Version__c> textVersions = new Map<Id, GHI_DR_Text_Version__c>(
            [SELECT Id, Text__c FROM GHI_DR_Text_Version__c WHERE Id in :tileTexts]);
        
        for(GHI_DR_Tile_Version__c tile : tiles) {
            for(Integer i = 1; i <= NUM_TILE_TEXTS; i++) {
                current = tile.get(fieldBase + i + fieldEnd);
                
                if(current != null) {
                    currentId = (Id) current;
                    
                    if(textVersions.containsKey(currentId)) {
                        tile.putSobject(fieldBase + i + relationship, textVersions.get(currentId));
                    }
                }
            }
        }        
        
        return tiles;
    }    
    
}