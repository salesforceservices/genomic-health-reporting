public with sharing class GHI_DR_FooterVersionUtil {
    
    public static void handleBeforeInsert(List<GHI_DR_Template_Footer_Version__c> triggerNew) {
        setVersionNumber(triggerNew);
        GHI_DR_NameUtil.generateFooterName(triggerNew);
    }
    
    public static void handleBeforeUpdate(List<GHI_DR_Template_Footer_Version__c> triggerNew) {
        populateTemplateVersion(triggerNew);
        GHI_DR_NameUtil.generateFooterName(triggerNew);
        updateTextTemplates(triggerNew);//, triggerOld);
    }
    
    public static void handleAfterInsert(List<GHI_DR_Template_Footer_Version__c> triggerNew, Map<Id, GHI_DR_Template_Footer_Version__c> triggerOld) {
        updateTextTemplates(triggerNew);//, triggerOld);
    }
    
    public static void handleAfterUpdate(List<GHI_DR_Template_Footer_Version__c> triggerNew, Map<Id, GHI_DR_Template_Footer_Version__c> triggerOld) {
        //updateTextTemplates(triggerNew, triggerOld);
        enforceReadOnly(triggerNew, triggerOld);
    }   
    
    public static List<GHI_DR_Template_Footer_Version__c> setVersionNumber(List<GHI_DR_Template_Footer_Version__c> footers) {
        Set<Id> parents = new Set<Id>();
        
        for(GHI_DR_Template_Footer_Version__c footer : footers) {
            parents.add(footer.GHI_DR_Template_Footer__c);
        }

        parents.remove(null);   
        
        List<AggregateResult> ars = 
            [SELECT GHI_DR_Template_Footer__c pid, MAX(Version_Number__c) num, Language__c lang 
               FROM GHI_DR_Template_Footer_Version__c 
              WHERE GHI_DR_Template_Footer__c in :parents 
           GROUP BY GHI_DR_Template_Footer__c, Language__c];
           
        Map<String, Integer> tvToCounts = new Map<String, Integer>();
        
        for(AggregateResult ar : ars) {
            tvToCounts.put((Id) ar.get('pid') + String.valueOf(ar.get('lang')), Integer.valueOf(ar.get('num')));
        }

        Integer currentVersion;
        String key;
        
        for(GHI_DR_Template_Footer_Version__c footer : footers) {
            key = footer.GHI_DR_Template_Footer__c + footer.Language__c;
            currentVersion = tvToCounts.containsKey(key) ?
                             tvToCounts.get(key) + 1 : 1;
            
            tvToCounts.put(key, currentVersion);
                             
            footer.Version_Number__c = currentVersion;
        }
        
        return footers;
    }    
    
    public static void enforceReadOnly(List<GHI_DR_Template_Footer_Version__c> footers, Map<Id, GHI_DR_Template_Footer_Version__c> old) {
        Set<Id> templateIds = new Set<Id>();
        for(GHI_DR_Template_Footer_Version__c footer : footers) {
            if(footer.Template_Version__c != null) {
                templateIds.add(footer.Template_Version__c);
            }
        }
        
        Map<Id, GHI_DR_Template_Version__c> idToTemplate = new Map<Id, GHI_DR_Template_Version__c>(
            [SELECT Id, Is_Template_Live__c 
               FROM GHI_DR_Template_Version__c 
              WHERE Approval_Status__c = :GHI_DR_TemplateUtil.STATUS_APPROVED AND Id in :templateIds]);

        for(GHI_DR_Template_Footer_Version__c footer : footers) {
            if(footer.Template_Version__c != null && idToTemplate.containsKey(footer.Template_Version__c)) {
                footer.addError(GHI_DR_TemplateVersionUtil.ALL_RO_ERROR);
            }
        }
    }
    
    public static List<GHI_DR_Template_Footer_Version__c> updateTextTemplates(List<GHI_DR_Template_Footer_Version__c> footers){//, 
                                                                              //Map<Id, GHI_DR_Template_Footer_Version__c> old) {
        
        Map<Id, Id> textToTemplate = new Map<Id, Id>();
        Id current;
        Id previous;

        for(GHI_DR_Template_Footer_Version__c footer : footers) {
            for(String field : GHI_DR_CloneUtil.TEXTS_FOOTER_VERSION) {
                current = (Id) footer.get(field);
                
                if(current != null) {
                    textToTemplate.put(current, footer.Template_Version__c);
                }
                
                /*
                previous = old != null && old.containsKey(footer.Id) ?
                           (Id) old.get(footer.Id).get(field) : null;
                
                if(current == null) {
                    if(previous != null) {
                        textToTemplate.put(previous, null);
                    }
                } else {
                    textToTemplate.put(current, footer.Template_Version__c);
                }*/
            }
        }

        GHI_DR_TextVersionUtil.updateTextTemplates(textToTemplate);
        return footers;
    }
    
    public static List<GHI_DR_Template_Footer_Version__c> populateTemplateVersion(List<GHI_DR_Template_Footer_Version__c> footers) {
        List<GHI_DR_Template_Version__c> templates = 
            [SELECT Id,
                    GHI_DR_Template_Footer_Version__c,
                    Language__c
               FROM GHI_DR_Template_Version__c 
              WHERE GHI_DR_Template_Footer_Version__c in :footers];
        
        Map<Id, GHI_DR_Template_Version__c> drawerToTemplate = new Map<Id, GHI_DR_Template_Version__c>();
        for(GHI_DR_Template_Version__c template : templates) {
            drawerToTemplate.put(template.GHI_DR_Template_Footer_Version__c, template);
        }
        
        for(GHI_DR_Template_Footer_Version__c footer : footers) {
            if(drawerToTemplate.containsKey(footer.Id)) {
                footer.Template_Version__c = drawerToTemplate.get(footer.Id).Id;
                footer.Language__c = drawerToTemplate.get(footer.Id).Language__c;                
            }
        }
        
        return footers;
    }
    
}