public with sharing class GHI_DR_NewTemplateVersionExtension {

    public GHI_DR_Template_Version__c templateVersion {get; set;}
    public Boolean cloning {get; set;}
    
    public GHI_DR_NewTemplateVersionExtension(ApexPages.StandardController ctrl) {
        cloning = false;
        
        List<GHI_DR_Template_Version__c> versions = 
            [SELECT Id, Name 
               FROM GHI_DR_Template_Version__c 
              WHERE Id = :ctrl.getId()];
              
        if(!versions.isEmpty()) {
            templateVersion = versions.get(0);
        }
    }
    
    public PageReference createNewVersion() {
        if(templateVersion != null && !cloning) {
            cloning = true;
            
            Id clonedRecordId = GHI_DR_CloneUtil.createNewTemplateVersion(templateVersion.Id);
            
            if(clonedRecordId != null) {
                return new PageReference('/' + clonedRecordId);
            }            
        }
        
        cloning = false;
        return null;
    }

}