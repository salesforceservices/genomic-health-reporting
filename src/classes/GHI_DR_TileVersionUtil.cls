public with sharing class GHI_DR_TileVersionUtil {
    
    public static void handleBeforeInsert(List<GHI_DR_Tile_Version__c> triggerNew) {
        setVersionNumber(triggerNew);
        GHI_DR_NameUtil.generateTileNames(triggerNew);
    }
    
    public static void handleBeforeUpdate(List<GHI_DR_Tile_Version__c> triggerNew) {
        GHI_DR_NameUtil.generateTileNames(triggerNew);
    }    
    
    public static void handleAfterInsert(List<GHI_DR_Tile_Version__c> triggerNew, Map<Id, GHI_DR_Tile_Version__c> triggerOld) {
        updateTextTemplates(triggerNew, triggerOld);
    }
    
    public static void handleAfterUpdate(List<GHI_DR_Tile_Version__c> triggerNew, Map<Id, GHI_DR_Tile_Version__c> triggerOld) {
        updateTextTemplates(triggerNew, triggerOld);
        enforceReadOnly(triggerNew, triggerOld);
    }   
    
    public static List<GHI_DR_Tile_Version__c> setVersionNumber(List<GHI_DR_Tile_Version__c> tiles) {
        Set<Id> parents = new Set<Id>();
        
        for(GHI_DR_Tile_Version__c tile : tiles) {
            parents.add(tile.GHI_DR_Tile__c);
        }

        parents.remove(null);
        
        List<AggregateResult> ars = 
            [SELECT GHI_DR_Tile__c pid, MAX(Version_Number__c) num, Language__c lang 
               FROM GHI_DR_Tile_Version__c 
              WHERE GHI_DR_Tile__c in :parents 
           GROUP BY GHI_DR_Tile__c, Language__c];
           
        Map<String, Integer> tvToCounts = new Map<String, Integer>();
        
        for(AggregateResult ar : ars) {
            tvToCounts.put((Id) ar.get('pid') + String.valueOf(ar.get('lang')), Integer.valueOf(ar.get('num')));
        }

        Integer currentVersion;
        String key;
        for(GHI_DR_Tile_Version__c tile : tiles) {
            key = tile.GHI_DR_Tile__c + tile.Language__c;
            currentVersion = tvToCounts.containsKey(key) ?
                             tvToCounts.get(key) + 1 : 1;
            
            tvToCounts.put(key, currentVersion);
                             
            tile.Version_Number__c = currentVersion;  
        }
        
        return tiles;
    }       
    
    public static void enforceReadOnly(List<GHI_DR_Tile_Version__c> tiles, Map<Id, GHI_DR_Tile_Version__c> old) {
        Set<Id> templateIds = new Set<Id>();
        for(GHI_DR_Tile_Version__c tile : tiles) {
            if(tile.GHI_DR_Template_Version__c != null) {
                templateIds.add(tile.GHI_DR_Template_Version__c);
            }
        }
        
        Map<Id, GHI_DR_Template_Version__c> idToTemplate = new Map<Id, GHI_DR_Template_Version__c>(
            [SELECT Id, Is_Template_Live__c 
               FROM GHI_DR_Template_Version__c 
              WHERE Approval_Status__c = :GHI_DR_TemplateUtil.STATUS_APPROVED AND Id in :templateIds]);

        for(GHI_DR_Tile_Version__c tile : tiles) {
            if(tile.GHI_DR_Template_Version__c != null && idToTemplate.containsKey(tile.GHI_DR_Template_Version__c)) {
                tile.addError(GHI_DR_TemplateVersionUtil.ALL_RO_ERROR);
            }
        }
    }    
    
    public static List<GHI_DR_Tile_Version__c> updateTextTemplates(List<GHI_DR_Tile_Version__c> tiles, Map<Id, GHI_DR_Tile_Version__c> old) {
        Map<Id, Id> textToTemplate = new Map<Id, Id>();
        List<GHI_DR_Text_Version__c> texts = new List<GHI_DR_Text_Version__c>();
        Id current;
        Id previous;

        for(GHI_DR_Tile_Version__c tile : tiles) {
            for(String field : GHI_DR_CloneUtil.TEXTS_TILE_VERSION) {
                current  = (Id) tile.get(field);
                previous = old != null && old.containsKey(tile.Id) ?
                           (Id) old.get(tile.Id).get(field) : null;
                
                if(current != previous) {
                    if(current == null) {
                        textToTemplate.put(previous, null);
                    } else {
                        textToTemplate.put(current, tile.GHI_DR_Template_Version__c);
                    }
                }
            }
        }

        GHI_DR_TextVersionUtil.updateTextTemplates(textToTemplate);         
        return tiles;
    }     
    
}