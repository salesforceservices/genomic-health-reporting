public with sharing class GHI_DR_TextVersionUtil {
    
    public static void handleBeforeInsert(List<GHI_DR_Text_Version__c> triggerNew) {
        setVersionNumber(triggerNew);
        GHI_DR_NameUtil.generateTextNames(triggerNew);
    }
    
    public static void handleBeforeUpdate(List<GHI_DR_Text_Version__c> triggerNew) {
        GHI_DR_NameUtil.generateTextNames(triggerNew);
    }    
    
    public static void handleAfterUpdate(List<GHI_DR_Text_Version__c> triggerNew, Map<Id, GHI_DR_Text_Version__c> triggerOld) {
        enforceReadOnly(triggerNew, triggerOld);
    }  
    
    public static List<GHI_DR_Text_Version__c> setVersionNumber(List<GHI_DR_Text_Version__c> texts) {
        Set<Id> parents = new Set<Id>();
        
        for(GHI_DR_Text_Version__c tv : texts) {
            parents.add(tv.GHI_DR_Text__c);
        }
        
        parents.remove(null);
        
        List<AggregateResult> ars = 
            [SELECT GHI_DR_Text__c pid, MAX(Version_Number__c) num, Language__c lang 
               FROM GHI_DR_Text_Version__c 
              WHERE GHI_DR_Text__c in :parents 
           GROUP BY GHI_DR_Text__c, Language__c];
           
        Map<String, Integer> tvToCounts = new Map<String, Integer>();
        
        for(AggregateResult ar : ars) {
            tvToCounts.put((Id) ar.get('pid') + String.valueOf(ar.get('lang')), Integer.valueOf(ar.get('num')));
        }
        
        Integer currentVersion;
        String key;
        for(GHI_DR_Text_Version__c tv : texts) {
            key = tv.GHI_DR_Text__c + tv.Language__c;
            currentVersion = tvToCounts.containsKey(key) ?
                             tvToCounts.get(key) + 1 : 1;
            
            tvToCounts.put(key, currentVersion);
                             
            tv.Version_Number__c = currentVersion; 
        }
        
        return texts;
    }    
    
    public static void enforceReadOnly(List<GHI_DR_Text_Version__c> texts, Map<Id, GHI_DR_Text_Version__c> old) {
        Set<Id> templateIds = new Set<Id>();
        for(GHI_DR_Text_Version__c text : texts) {
            if(text.Template_Version__c != null) {
                templateIds.add(text.Template_Version__c);
            }
        }
        
        Map<Id, GHI_DR_Template_Version__c> idToTemplate = new Map<Id, GHI_DR_Template_Version__c>(
            [SELECT Id, Is_Template_Live__c 
               FROM GHI_DR_Template_Version__c 
              WHERE Approval_Status__c = :GHI_DR_TemplateUtil.STATUS_APPROVED AND Id in :templateIds]);

        for(GHI_DR_Text_Version__c text : texts) {
            if(text.Template_Version__c != null && idToTemplate.containsKey(text.Template_Version__c)) {
                text.addError(GHI_DR_TemplateVersionUtil.ALL_RO_ERROR);
            }
        }
    }
    
    public static void updateTextTemplates(Map<Id, Id> textToTemplate) {
        List<GHI_DR_Text_Version__c> texts = new List<GHI_DR_Text_Version__c>();
        Id templateId;
        String language;
        
        Map<Id, GHI_DR_Template_Version__c> templateToLanguage = new Map<Id, GHI_DR_Template_Version__c>(
            [SELECT Id, Language__c 
               FROM GHI_DR_Template_Version__c 
              WHERE Id in :textToTemplate.values()]);
        
        for(Id tvId : textToTemplate.keySet()) {
            templateId = textToTemplate.get(tvId);
            language = templateToLanguage.containsKey(templateId) && 
                       templateToLanguage.get(templateId) != null ?
                       templateToLanguage.get(templateId).Language__c : null;
                       
            texts.add(new GHI_DR_Text_Version__c(
                Id = tvId,
                Template_Version__c = templateId,
                Language__c = language
            ));
        }
                
        update texts;        
    }
    
}