public class GHI_DR_DrawerGroupVersionUtil {
    
    public static void handleBeforeInsert(List<GHI_DR_Drawer_Group_Version__c> triggerNew) {
        setVersionNumber(triggerNew);
        GHI_DR_NameUtil.generateDrawerGroupNames(triggerNew);
    }
    
    public static void handleBeforeUpdate(List<GHI_DR_Drawer_Group_Version__c> triggerNew) {
        populateTemplateVersion(triggerNew);
        GHI_DR_NameUtil.generateDrawerGroupNames(triggerNew);
    }    
    
    public static void handleAfterUpdate(List<GHI_DR_Drawer_Group_Version__c> triggerNew, Map<Id, GHI_DR_Drawer_Group_Version__c> triggerOld) {
        enforceReadOnly(triggerNew, triggerOld);
    }
    
    public static List<GHI_DR_Drawer_Group_Version__c> setVersionNumber(List<GHI_DR_Drawer_Group_Version__c> drawerGroups) {
        Set<Id> groups = new Set<Id>();
        
        for(GHI_DR_Drawer_Group_Version__c gv : drawerGroups) {
            groups.add(gv.GHI_DR_Drawer_Group__c);
        }
        
        groups.remove(null);
        
        List<AggregateResult> ars = 
            [SELECT GHI_DR_Drawer_Group__c dg, MAX(Version_Number__c) num, Language__c lang 
               FROM GHI_DR_Drawer_Group_Version__c 
              WHERE GHI_DR_Drawer_Group__c in :groups 
           GROUP BY GHI_DR_Drawer_Group__c, Language__c];
           
        Map<String, Integer> tvToCounts = new Map<String, Integer>();
        
        for(AggregateResult ar : ars) {
            tvToCounts.put((Id) ar.get('dg') + String.valueOf(ar.get('lang')), Integer.valueOf(ar.get('num')));
        }     
        
        Integer currentVersion;
        String key;
        for(GHI_DR_Drawer_Group_Version__c gv : drawerGroups) {
            key = gv.GHI_DR_Drawer_Group__c + gv.Language__c;
            currentVersion = tvToCounts.containsKey(key) ?
                             tvToCounts.get(key) + 1 : 1;
            
            tvToCounts.put(key, currentVersion);
                             
            gv.Version_Number__c = currentVersion;
        }
        
        return drawerGroups;
    }
    
    public static void enforceReadOnly(List<GHI_DR_Drawer_Group_Version__c> dgvs, Map<Id, GHI_DR_Drawer_Group_Version__c> old) {
        Set<Id> templateIds = new Set<Id>();
        for(GHI_DR_Drawer_Group_Version__c dgv : dgvs) {
            if(dgv.Template_Version__c != null) {
                templateIds.add(dgv.Template_Version__c);
            }
        }
        
        Map<Id, GHI_DR_Template_Version__c> idToTemplate = new Map<Id, GHI_DR_Template_Version__c>(
            [SELECT Id, Is_Template_Live__c 
               FROM GHI_DR_Template_Version__c 
              WHERE Approval_Status__c = :GHI_DR_TemplateUtil.STATUS_APPROVED AND Id in :templateIds]);

        for(GHI_DR_Drawer_Group_Version__c dgv : dgvs) {
            if(dgv.Template_Version__c != null && idToTemplate.containsKey(dgv.Template_Version__c)) {
                dgv.addError(GHI_DR_TemplateVersionUtil.ALL_RO_ERROR);
            }
        }
    }    
    
    private static List<GHI_DR_Drawer_Group_Version__c> populateTemplateVersion(List<GHI_DR_Drawer_Group_Version__c> dgvs) {
        List<GHI_DR_Template_Version__c> templates = 
            [SELECT Id,
                    GHI_DR_Template_Header_Version__r.Drawer_Group_1__c,
                    GHI_DR_Template_Header_Version__r.Drawer_Group_2__c,
                    GHI_DR_Template_Header_Version__r.Drawer_Group_3__c,
                    Language__c
               FROM GHI_DR_Template_Version__c 
              WHERE GHI_DR_Template_Header_Version__r.Drawer_Group_1__c in :dgvs OR 
                    GHI_DR_Template_Header_Version__r.Drawer_Group_2__c in :dgvs OR
                    GHI_DR_Template_Header_Version__r.Drawer_Group_3__c in :dgvs];
                    
        Map<String, GHI_DR_Template_Version__c> drawerToTemplate = new Map<String, GHI_DR_Template_Version__c>();
        
        for(GHI_DR_Template_Version__c template : templates) {
            if(template.GHI_DR_Template_Header_Version__r.Drawer_Group_1__c != null) {
                drawerToTemplate.put(template.GHI_DR_Template_Header_Version__r.Drawer_Group_1__c, template);
            }
            
            if(template.GHI_DR_Template_Header_Version__r.Drawer_Group_2__c != null) {
                drawerToTemplate.put(template.GHI_DR_Template_Header_Version__r.Drawer_Group_2__c, template);
            }
            
            if(template.GHI_DR_Template_Header_Version__r.Drawer_Group_3__c != null) {
                drawerToTemplate.put(template.GHI_DR_Template_Header_Version__r.Drawer_Group_3__c, template);
            }
        }
        
        for(GHI_DR_Drawer_Group_Version__c dgv : dgvs) {
            if(drawerToTemplate.containsKey(dgv.Id)) {
                dgv.Template_Version__c = drawerToTemplate.get(dgv.Id).Id;
                dgv.Language__c = drawerToTemplate.get(dgv.Id).Language__c;                  
            }
        }
        
        return dgvs;
    }
    
}