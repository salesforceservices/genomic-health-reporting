public with sharing class GHI_DR_TemplateController {

    private static final String TEMPLATE_ID_PARAM     = 'tid';
    private static final String RESULT_ID_PARAM       = 'rid';
    private static final String FORMAT_PARAM          = 'type';
    private static final String SCOPE_PARAM           = 'scope';
    private static final String DEFAULT_REPORT_FORMAT = 'Web';
    
    public Id templateId       {get; set;}
    public Id reportId         {get; set;}
    public String reportFormat {get; set;}
    public String sessionId    {get; set;}
    public String instance     {get; set;}
    public String scope        {get; set;}
    
    public GHI_DR_TemplateController() {
        templateId   = ApexPages.currentPage().getParameters().get(TEMPLATE_ID_PARAM);
        reportId     = ApexPages.currentPage().getParameters().get(RESULT_ID_PARAM);
        reportFormat = ApexPages.currentPage().getParameters().get(FORMAT_PARAM);
        scope        = ApexPages.currentPage().getParameters().get(SCOPE_PARAM);
        sessionId    = UserInfo.getSessionId();
        instance     = URL.getSalesforceBaseUrl().getProtocol() + '://' + URL.getSalesforceBaseUrl().getHost();

        if(String.isBlank(reportFormat)) {
            reportFormat = DEFAULT_REPORT_FORMAT;
        }
        
        if(String.isNotBlank(reportId) && String.isBlank(templateId)) {
            templateId = GHI_DR_TemplateUtil.setTemplateByResults(reportId, reportFormat);
        }
    }

    @RemoteAction
    public static GHI_DR_TemplateInfo getTemplate(Id reportId, Id templateId) {
        return new GHI_DR_TemplateInfo(reportId, templateId);
    }
    
    @RemoteAction
    public static void setPDFPreference(String preference) {
        update new User(
            Id = UserInfo.getUserId(),
            GHI_DR_PDF_Preference__c = preference
        );
    }
    
    @RemoteAction
    public static String getPDF(String endpoint) {
        GHI_DR_Settings__c orgSettings = GHI_DR_Settings__c.getOrgDefaults();
        
        String requestEndpoint = orgSettings.Phantom_URL__c + encrypt(endpoint);
    
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(requestEndpoint);
        request.setMethod('GET');
        request.setTimeout(120000);
        
        Http callout = new Http();
        HTTPResponse response = callout.send(request);
        return EncodingUtil.base64Encode(response.getBodyAsBlob());
    }
    
    @RemoteAction
    public static String encrypt(String toEncrypt) {
        String timeStampInSeconds = String.valueOf((DateTime.now().getTime()/1000));
        return GHI_DR_TemplateUtil.aes256Encrypt(timeStampInSeconds + ' ' + toEncrypt);
    }
}