@isTest
private class GHI_DR_OrdersTest {

	private static testMethod void testGetReports() {
	    Test.startTest();
	    
        List<User> integrationUsers =
            [SELECT Id FROM User WHERE GHI_DR_Integration_User__c = true LIMIT 1];
            
	    Id standardPricebookId = Test.getStandardPricebookId();
	    Account testAcct = GHI_DR_TestUtil.createAccount('acme');
	    Order testOrder = GHI_DR_TestUtil.createOrder('to', 'IBC', testAcct.Id, standardPricebookId);
	    
	    OSM_Result__c testOSMResult = GHI_DR_TestUtil.createOSMResult('Results');

	    Product2 testProd = OSM_DataFactory.createProduct('ibc report', true);
	    insert testProd;
	    
	    PricebookEntry testPBE = OSM_DataFactory.createPricebookEntry(testProd.Id, standardPricebookId, 1);
	    insert testPBE;

	    OrderItem testItem = 
	        GHI_DR_TestUtil.createOrderItem('IBC', 
	                                        'Node Negative', 
	                                        testOrder.Id, 
	                                        testOSMResult.Id,
	                                        testPBE.Id);

	    OSM_Distribution_Event__c testDist = 
	        GHI_DR_TestUtil.createDistributionEvent('Portal', 
	                                                'Success', 
	                                                'English', 
	                                                '/report/url', 
	                                                'fake-report-id', 
	                                                testOrder.Id, 
	                                                testItem.Id,
	                                                testOSMResult.Id, 
	                                                'test-dist-1',
	                                                null);

	    GHI_DR_Template_Header__c testHeader = GHI_DR_TestUtil.createHeader('test-header');
	    GHI_DR_Template_Footer__c testFooter = GHI_DR_TestUtil.createFooter('test-footer');
	    GHI_DR_Template__c testTemplate = GHI_DR_TestUtil.createTemplate('test-template', 'IBC', 'Web');
	    
	    GHI_DR_Template_Header_Version__c testHeaderVersion = 
	        GHI_DR_TestUtil.createHeaderVersion('thv', 'thvComponent', testHeader.Id);
	        
	    GHI_DR_Template_Footer_Version__c testFooterVersion = 
	        GHI_DR_TestUtil.createFooterVersion('tfv', 'tfvComponent', testFooter.Id);
	        
	    GHI_DR_Template_Version__c testTemplateVersion = 
	        GHI_DR_TestUtil.createTemplateVersion('ttv', testTemplate.Id, testHeaderVersion.Id, testFooterVersion.Id, 'GHI_DR_WebTemplate');

	    GHI_DR_Result_Set__c testResult = 
	        GHI_DR_TestUtil.createResultSet(GHI_DR_TestUtil.RESULT_TYPE_BREAST, 
	                                       'fake-report-id',
	                                       testOrder.Id,
	                                       'test-dist-1');

        System.runAs(integrationUsers.get(0)) {
    	    testResult.Digital_Template__c = testTemplateVersion.Id;
    	    update testResult;            
        }
	    
        Test.stopTest();
        
        List<GHI_DR_Result_Set__c> results = GHI_DR_Orders.getReports();
        System.assertEquals(1, results.size());
	}

}