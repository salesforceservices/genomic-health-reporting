@isTest
private class GHI_DR_MessageProcessorTest {

	private static testMethod void testBreastReportParse() {
	    Integer count = [SELECT count() FROM GHI_DR_Result_Set__c LIMIT 1];
	    
	    System.assertEquals(0, count);
	    
        ESBInboundMessage__c msg = GHI_DR_TestUtil.createInboundMessage(
            'ResultsDataMessage', 
            GHI_DR_TestUtil.getInboundMessageBreastXML(),
            '4b12543d-8fa2-4daf-879d-57a55e41b061');
            
        count = [SELECT count() FROM GHI_DR_Result_Set__c LIMIT 1];    
            
        System.assertEquals(1, count);
	}
	
	private static testMethod void testProstateReportParse() {
	    Integer count = [SELECT count() FROM GHI_DR_Result_Set__c LIMIT 1];
	    
	    System.assertEquals(0, count);
	    
        ESBInboundMessage__c msg = GHI_DR_TestUtil.createInboundMessage(
            'ResultsDataMessage', 
            GHI_DR_TestUtil.getInboundMessageProstateXML(),
            '');
            
        count = [SELECT count() FROM GHI_DR_Result_Set__c LIMIT 1];    
            
        System.assertEquals(1, count);
	}	

}